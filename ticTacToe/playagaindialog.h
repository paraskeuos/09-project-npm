#ifndef PLAYAGAINDIALOG_H
#define PLAYAGAINDIALOG_H

#include <QDialog>

namespace Ui {
class PlayAgainDialog;
}

class PlayAgainDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PlayAgainDialog(QWidget *parent = nullptr);
    ~PlayAgainDialog();

private:
    Ui::PlayAgainDialog *ui;
};

#endif // PLAYAGAINDIALOG_H
