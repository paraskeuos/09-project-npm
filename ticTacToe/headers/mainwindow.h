#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "gameclient.h"
#include "tictactoewidget.h"

#include <QAbstractSocket>
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void startStopGame();
    void gameOver(TicTacToeWidget::Player result);

    void error(QAbstractSocket::SocketError socketError);
    void connectedToServer();
    void disconnectedFromServer();
    void gameInit(int player, int gameIndex);
    void sendMove(QJsonObject& move);
    void opponentMove(int move);
    void onNewGameOffer();
    void onGameInterrupted();

private:
    void logMessage(const QString& msg);

private:
    Ui::MainWindow *ui;
    GameClient* m_gameClient;


};
#endif // MAINWINDOW_H
