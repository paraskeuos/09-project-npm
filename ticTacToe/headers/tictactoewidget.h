#ifndef TICTACTOEWIDGET_H
#define TICTACTOEWIDGET_H

#include <QPushButton>
#include <QVector>
#include <QWidget>
#include <QJsonObject>

class TicTacToeWidget : public QWidget
{
    Q_OBJECT
public:
    explicit TicTacToeWidget(QWidget *parent = nullptr);

    enum Player {
        Invalid, Player1, Player2, Draw
    };
    Q_ENUM(Player)

    Player currentPlayer() const;
    void currentPlayer(Player p);
    void player(int p);
    int gameIndex() const;
    void gameIndex(const int gameIndex);

    void initNewGame();
    void opponentMove(const int index);
    void clearBoard();

signals:
    void currentPlayerChanged();
    void gameOver(Player);
    void sendMove(QJsonObject& move);

private slots:
    void handleButtonClick(int index);

private:
    Player checkCondition() const;

private:
    QVector<QPushButton*> m_board;
    Player m_currentPlayer;
    Player m_player;
    int m_gameIndex;
};

#endif // TICTACTOEWIDGET_H
