#include "headers/mainwindow.h"
#include "./ui_mainwindow.h"

#include <QHostAddress>
#include <QString>
#include <QDebug>
#include <playagaindialog.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_gameClient(new GameClient(this))
{
    ui->setupUi(this);
    connect(ui->startStopButton, &QPushButton::clicked,
            this, &MainWindow::startStopGame);
    connect(ui->gameboard, &TicTacToeWidget::gameOver,
            this, &MainWindow::gameOver);

    connect(m_gameClient, &GameClient::connected,
              this, &MainWindow::connectedToServer);
    connect(m_gameClient, &GameClient::disconnected,
            this, &MainWindow::disconnectedFromServer);
    connect(m_gameClient, &GameClient::error,
            this, &MainWindow::error);
    connect(m_gameClient, &GameClient::gameInit,
            this, &MainWindow::gameInit);
    connect(ui->gameboard, &TicTacToeWidget::sendMove,
            this, &MainWindow::sendMove);
    connect(m_gameClient, &GameClient::opponentMove,
            this, &MainWindow::opponentMove);
    connect(m_gameClient, &GameClient::newGameOffer,
            this, &MainWindow::onNewGameOffer);
    connect(m_gameClient, &GameClient::gameInterrupted,
            this, &MainWindow::onGameInterrupted);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::startStopGame()
{
    ui->startStopButton->setEnabled(false);

    if(ui->startStopButton->text().compare(tr("Start Game")) == 0) {

        if(m_gameClient->isConnected()) {
            m_gameClient->disconnect();
        }

        const QString hostAddress = "127.0.0.1";
        const int port = 1967;
        ui->startStopButton->setEnabled(false);
        m_gameClient->connectToServer(QHostAddress(hostAddress), port);

        ui->playerLabel->setText(tr("Player /"));
        ui->startStopButton->setText(tr("Stop Game"));

    } else {

        if(m_gameClient->isConnected()) {
            m_gameClient->disconnect();
        }

        ui->startStopButton->setText(tr("Start Game"));
    }

    ui->startStopButton->setEnabled(true);
}

void MainWindow::gameOver(TicTacToeWidget::Player result)
{
    switch(result) {
    case TicTacToeWidget::Draw: {
        logMessage(tr("It's a draw."));
        break;
    }
    case TicTacToeWidget::Player1:
    case TicTacToeWidget::Player2: {
        QString player = result == TicTacToeWidget::Player1 ? tr("X") : tr("O");
        logMessage(tr("Player ") + player + tr(" won!"));
        break;
    }
    default:
        break;
    }

    ui->playerLabel->setText(tr("Player /"));
    ui->startStopButton->setText(tr("Start Game"));
}

void MainWindow::connectedToServer()
{
    logMessage(tr("Connected to server."));
    ui->startStopButton->setText(tr("Stop Game"));

    ui->gameboard->initNewGame();
}

void MainWindow::disconnectedFromServer()
{
    logMessage(tr("Disconnected from server."));
    ui->startStopButton->setText(tr("Start Game"));
}

void MainWindow::gameInit(int player, int gameIndex)
{
    char playerSymbol = player == TicTacToeWidget::Player1 ? 'X' : 'O';
    ui->gameboard->player(player);
    ui->gameboard->gameIndex(gameIndex);

    logMessage(tr("gameInit, you're Player %1").arg(player));
    logMessage(tr("Game index: %1").arg(gameIndex));
    ui->playerLabel->setText(tr("Player %1").arg(playerSymbol));

    ui->gameboard->currentPlayer(TicTacToeWidget::Player1);
}

void MainWindow::logMessage(const QString &msg)
{
    ui->logEditor->appendPlainText(msg);
}

void MainWindow::sendMove(QJsonObject& move)
{
    m_gameClient->sendJson(move);
}

void MainWindow::opponentMove(int move)
{
    logMessage("Your opponent just made a new move.");
    ui->gameboard->opponentMove(move);
}

void MainWindow::onNewGameOffer()
{
    PlayAgainDialog dialog(this);
    if(dialog.exec() == QDialog::Rejected) {
        m_gameClient->disconnect();
        return;
    }

    ui->gameboard->initNewGame();
}

void MainWindow::onGameInterrupted()
{
    ui->playerLabel->setText(tr("Player: /"));
    ui->gameboard->clearBoard();
    logMessage("Your opponent disconnected. You have been requeued.");
}

void MainWindow::error(QAbstractSocket::SocketError socketError)
{
    QString errorMsg;
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
    case QAbstractSocket::ProxyConnectionClosedError:
        return; // disconnectedFromServer()
    case QAbstractSocket::ConnectionRefusedError:
        errorMsg = tr("ERROR: The host refused the connection.");
        break;
    case QAbstractSocket::ProxyConnectionRefusedError:
        errorMsg = tr("ERROR: The proxy refused the connection.");
        break;
    case QAbstractSocket::ProxyNotFoundError:
        errorMsg = tr("ERROR: Could not find the proxy.");
        break;
    case QAbstractSocket::HostNotFoundError:
        errorMsg = tr("ERROR: Could not find the server.");
        break;
    case QAbstractSocket::SocketAccessError:
        errorMsg = tr("ERROR: You don't have permissions to execute this operation.");
        break;
    case QAbstractSocket::SocketResourceError:
        errorMsg = tr("ERROR: Too many connections opened.");
        break;
    case QAbstractSocket::SocketTimeoutError:
        errorMsg = tr("ERROR: Operation timed out.");
        return;
    case QAbstractSocket::ProxyConnectionTimeoutError:
        errorMsg = tr("ERROR: Proxy timed out.");
        break;
    case QAbstractSocket::NetworkError:
        errorMsg = tr("ERROR: Unable to reach the network.");
        break;
    case QAbstractSocket::UnknownSocketError:
        errorMsg = tr("ERROR: An unknown error occured.");
        break;
    case QAbstractSocket::UnsupportedSocketOperationError:
        errorMsg = tr("ERROR: Operation not supported.");
        break;
    case QAbstractSocket::ProxyAuthenticationRequiredError:
        errorMsg = tr("ERROR: Your proxy requires authentication.");
        break;
    case QAbstractSocket::ProxyProtocolError:
        errorMsg = tr("ERROR: Proxy comunication failed.");
        break;
    case QAbstractSocket::TemporaryError:
    case QAbstractSocket::OperationError:
        errorMsg = tr("ERROR: Operation failed, please try again.");
        break;
    default:
        Q_UNREACHABLE();
        break;
    }

    logMessage(errorMsg);
    ui->startStopButton->setText(tr("Start Game"));
}
