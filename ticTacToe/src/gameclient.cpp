#include "../headers/gameclient.h"

#include <QByteArray>
#include <QDataStream>
#include <QJsonDocument>
#include <QJsonParseError>

GameClient::GameClient(QObject *parent)
    : QObject(parent)
    , m_clientSocket(new QTcpSocket(this))
{
    connect(m_clientSocket, &QTcpSocket::connected, this, &GameClient::connected);
    connect(m_clientSocket, &QTcpSocket::disconnected, this, &GameClient::disconnected);
    connect(m_clientSocket, &QTcpSocket::readyRead, this, &GameClient::onReadyRead);
    connect(m_clientSocket,
            QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::errorOccurred),
            this, &GameClient::error);
}

void GameClient::disconnect()
{
    m_clientSocket->disconnectFromHost();
}

bool GameClient::isConnected()
{
    return m_clientSocket->state() == QAbstractSocket::ConnectedState;
}

void GameClient::connectToServer(const QHostAddress &address, quint16 port)
{
    m_clientSocket->connectToHost(address, port);
}

void GameClient::sendJson(const QJsonObject& jsonObj)
{
    Q_ASSERT(m_clientSocket);
    const QByteArray jsonData = QJsonDocument(jsonObj).toJson(QJsonDocument::Compact);

    QDataStream socketStream(m_clientSocket);
    socketStream.setVersion(QDataStream::Qt_5_12);
    socketStream << jsonData;
}

void GameClient::jsonReceived(const QJsonObject &jsonObj)
{
    const QJsonValue typeVal = jsonObj.value("type");
    if(typeVal.isNull() || !typeVal.isString()) {
        return;
    }

    if(typeVal.toString().compare("gameInit", Qt::CaseInsensitive) == 0) {
        const QJsonValue playerVal = jsonObj.value("player");

        if(playerVal.isNull() || !playerVal.isDouble()) {
            return;
        }
        const int player = playerVal.toInt();

        const QJsonValue gameIndexVal = jsonObj.value("gameIndex");

        if(gameIndexVal.isNull() || !gameIndexVal.isDouble()) {
            return;
        }
        const int gameIndex = gameIndexVal.toInt();

        emit gameInit(player, gameIndex);

    } else if(typeVal.toString().compare("play", Qt::CaseInsensitive) == 0) {
        const QJsonValue moveVal = jsonObj.value("move");

        if(moveVal.isNull() || !moveVal.isDouble()) {
            return;
        }

        const int move = moveVal.toInt();

        emit opponentMove(move);

    } else if(typeVal.toString().compare("playAgain", Qt::CaseInsensitive) == 0) {
        emit newGameOffer();

    } else if(typeVal.toString().compare("gameInterrupted", Qt::CaseInsensitive) == 0) {
        emit gameInterrupted();
    }
}

void GameClient::onReadyRead()
{
    QByteArray jsonData;
    QDataStream socketStream(m_clientSocket);
    socketStream.setVersion(QDataStream::Qt_5_12);

    while(true) {
        socketStream.startTransaction();
        socketStream >> jsonData;
        if(socketStream.commitTransaction()) {

            QJsonParseError parseError;
            const QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData, &parseError);
            if(parseError.error == QJsonParseError::NoError && jsonDoc.isObject()) {

                 jsonReceived(jsonDoc.object());

            }

        } else {
            break;
        }
    }
}


