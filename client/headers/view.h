#ifndef VIEW_H
#define VIEW_H

#include <QGraphicsView>
#include <QMouseEvent>
#include <memory>

#include <headers/dashboard/dashboard.h>
#include <headers/games/game.h>

class View : public QGraphicsView
{
public:
    View();
    ~View();

private:
    const QImage m_background = QImage(":assets/images/background.jpg");
    const int m_width = m_background.width() ;
    const int m_height = m_background.height();
    std::unique_ptr<Dashboard> m_dashboard;
    //std::unique_ptr<Game> m_game;
    Game* m_game;

    // QWidget interface
protected:
    void mousePressEvent(QMouseEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;

};
#endif // VIEW_H
