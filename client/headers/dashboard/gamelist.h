#ifndef GAMELIST_H
#define GAMELIST_H

#include <QGraphicsItem>
#include <QGraphicsSceneWheelEvent>
#include <QKeyEvent>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QTimer>
#include <QGraphicsScene>
#include <QWidget>

class GameList : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:
    GameList(qreal gameItemSize, size_t n, qreal width, qreal height);

    // QGraphicsItem interface
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    //funkcija koja omogucava pomeranje slicica igara preko tastature
    //tako sto emituje signal wheelEvent i onda se dobije isto ponasanje
    //kao da je skrolovanje u pitanju
    void callWheel(bool);
    // funkcija koja se poziva u specijalnom slucaju kada nijedna slicica nije odabrana,
    // a pritisnuli smo taster na levo ili desno
    void resetGameListPosition();

public slots:
    // Slotovi za animacije
    void scrollLeft();
    void scrollRight();

    // QGraphicsItem interface
protected:
    void wheelEvent(QGraphicsSceneWheelEvent *event) override;

private:
    // Velicina neuvecane slike igre
    qreal m_gameItemSize;

    // Rastojanje izmedju centara kvadrata/slika
    qreal m_step;

    // Koristi se kao cilj scroll animacija
    qreal m_nextX;

    // Granicne vrednosti za animacije skrolovanja
    qreal m_minX;
    qreal m_maxX;

    // Broj igara u listi
    size_t m_gameNum;

    // Celokupni okvir
    QRectF m_rect;

    qreal m_width, m_height;

    // Tajmeri za animacije skrolovanja
    QTimer* m_timerLeft = nullptr;
    QTimer* m_timerRight = nullptr;

};

#endif // GAMELIST_H
