#ifndef GAMEDESCRIPTIONBOX_H
#define GAMEDESCRIPTIONBOX_H

#include <QGraphicsItem>
#include <QObject>

class GameDescriptionBox: public QGraphicsItem
{
public:
    GameDescriptionBox(QRectF rect);

    // QGraphicsItem interface
public:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    void setGameName(QString gameName);

private:
    QRectF m_rect;
    QString m_gameName;
    QRectF rect;
};

#endif // GAMEDESCRIPTIONBOX_H
