#ifndef GAMEITEM_H
#define GAMEITEM_H

#include <QGraphicsItem>
#include <QGraphicsSceneHoverEvent>
#include <QImage>
#include <QPainter>
#include <QRectF>
#include <QString>
#include <QStyleOptionGraphicsItem>
#include <QTimer>
#include <QWidget>
#include <headers/dashboard/gamedescriptionbox.h>

class GameItem : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:
    // Za implementaciju funkcije type()
    enum { StartGame = UserType };

    // Na osnovu ove vrednosti se pokrece odgovarajuca igra
    enum GameId { Connect4, Sibicarenje, Game3, GridPolitics, Smash_It, Game6};

    GameItem(GameId gameId, qreal size, QImage image, GameDescriptionBox* gameDescription);

    ~GameItem();

    GameId gameId() const;
    void setRect(QRectF& rect);

    // QGraphicsItem interface
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    int type() const override;

    // preko metoda moveGameIcon umanjujemo prethodno odabranu slicicu
    // i uvecavamo sledecu
    void moveGameIcon(bool);

    bool isExpanded();
    void setIsExpanded(bool flag);
    bool toExpand();
    void setToExpand(bool flag);

public slots:
     // expand() je slot za hoverEnter, shrink za hoverLeave dogadjaj
    void expand();
    void shrink();

    // QGraphicsItem interface
protected:
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override;

private:
    GameId m_gameId;
    qreal m_size;
    QRectF m_rect;
    QImage m_img;

    // Promenljive potrebne za animacije prilikom hoverEnter i hoverLeave dogadjaja
    QRectF m_minRect;
    QRectF m_maxRect;
    QTimer* m_timer = nullptr;

    // zastavica koja ukazuje da li je polje vec prosireno
    bool m_isExpanded;
    // zastavica koja ukazuje da li polje treba prosiriti
    bool m_toExpand;

    void showGameDescription();
    void hideGameDescription();

    // staticka promenljiva koja ukazuje da li treba crtati samu kutiju
    // u kojoj se nalazi opis svake igre
    // postavljena je kao staticka da ne bi svaka igra crtala svoju kutiju,
    // a s druge strane nije jedinstvena za svaku igru jer bi onda morala
    // da se brise pri menjanju svake slicice sto bi dovelo do ,,treperenja"
    // kad god se promeni tekuca igra
    constexpr static bool descriptionBox = false;

    GameDescriptionBox* m_descriptionBox;

    // QGraphicsItem interface
protected:
    void wheelEvent(QGraphicsSceneWheelEvent *event) override;
};

#endif // GAMEITEM_H
