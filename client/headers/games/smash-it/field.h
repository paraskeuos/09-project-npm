#ifndef FIELD_H
#define FIELD_H

#include <QGraphicsObject>

// enumeracioni tip koji odredjuje status polja, samim tim i izgled
enum Status{
    NO_EFFECT, // polje je aktivirano, ali nema efekta
    NORMAL_NOT_ACTIVE, // polje nije aktivirano i nije aktivno tj nismo misem iznad njega
    NORMAL_ACTIVE, // polje nije aktivirano ali smo misem iznad njega
    MINUS_10_POINTS_NOT_PRESSED, // polje koje oduzima 10 poena koje nije pritisnuto
    MINUS_10_POINTS_PRESSED, // polje koje oduzima 10 poena i koje je pritisnuto
    PLUS_5_POINTS_NOT_PRESSED,
    PLUS_5_POINTS_PRESSED,
    DISABLE_ROW, // specijalno polje koje sluzi za onemogucavanje svih polja u istom redu
    DISABLE_COLUMN,
    DISABLE_MAIN_DIAGONAL,
    DISABLE_ANTI_DIAGONAL,
    PLUS_5_SECONDS, // specijalno polje koje dodaje 10 sekundi
    MINUS_10_SECONDS,
    ZERO_POINTS // specijalno polje koje vraca broj bodova na 0
};

// enumeracioni tip pravca koji se koristi za funkciju changeStatusAlongTheDirection
enum Direction{
    MAIN_DIAGONAL,
    ANTI_DIAGONAL,
    ROW,
    COLUMN
};

class Field: public QGraphicsObject
{
    Q_OBJECT
public:
    Field(qint16 antiDiagonalNumber, qint16 mainDiagonalNumber,
          qint16 rowNumber, qint16 columnNumber, Status status);

signals:
    void addPoints(qint32 points);
    void deductPoints(qint32 points);
    void increaseSecondsLeft(qint32 points);
    void decreaseSecondsLeft(qint32 points);
    // signal koji ce emitovati polja sa statusom
    // DISABLE_ROW, DISABLE_COLUMN, DISABLE_MAIN_DIAGONAL i DISABLE_ANTI_DIAGONAL
    // zelimo da putem nekog pravca (enum Direction) promenimo ,,nasilno" svim poljima status
    // number se koristi za odredjivanje koja je npr. glavna dijagonala u pitanju, a msec
    // odredjuje na koliko milisekundi treba da polje ima taj status
    // da se ne bi spajalo svako polje sa svakim polje, ovaj signal se salje klasi Smash_It, odakle
    // se onda salje signal forceChangeAlongTheDirection na koji se onda reaguje sa slotom
    // onForceChangeAlongTheDirection
    void changeStatusAlongTheDirection(Direction changeType, qint16 number, const Status &status, int msec);
    void eraseAllPoints();

public slots:
    void changeStatus(Field* field, const Status &status, int msec);
    void onForceChangeAlongTheDirection(Direction changeType, qint16 number, const Status &status, int msec);

private slots:
    // specijalan privatan slot kojim vracamo polja u status NORMAL_NOT_ACTIVE kad istekne tajmer za to polje
    void backToNormalNotActiveStatus();

public:
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    Status status() const;
    void setStatus(const Status &status);

protected:
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;

private:
    bool gameEnded() const;
    bool highScoreBeatenAnimationActive() const;

    QColor m_color;
    QTimer* m_timer;

    const QRgb LIGHT_GOLD = QColor(255, 215, 0).rgb();
    const QRgb DARK_GOLD = QColor(240, 190, 0).rgb();
    const QRgb LIGHT_CRIMSON = QColor(255,40,75).rgb();
    const QRgb CRIMSON = QColor(210,40,75).rgb();
    const QRgb LIGHT_LIME_GREEN = QColor(50,235,50).rgb();
    const QRgb LIME_GREEN = QColor(50,205,50).rgb();
    const QRgb GRAY = QColor(128,128,128).rgb();
    const QRgb DARK_GRAY = QColor(169,169,169).rgb();
    const QRgb DISABLE_ROW_COLOR = QColor(0, 135, 111).rgb();
    const QRgb DISABLE_COLUMN_COLOR = QColor(160, 120, 160).rgb();
    const QRgb DISABLE_MAIN_DIAGONAL_COLOR = QColor(100, 135, 11).rgb();
    const QRgb DISABLE_ANTI_DIAGONAL_COLOR = QColor(200, 11, 200).rgb();
    const QRgb BLACK = QColor(0,0,0).rgb();

    // polje je odredjeno na terenu brojem svoje glavne i sporedne dijagonale,
    // kao i broje reda i kolone; ove promenljive se koriste u funkciji onForceChangeAlongTheDirection
    const qint16 m_antiDiagonalNumber, m_mainDiagonalNumber;
    const qint16 m_rowNumber, m_columnNumber;
    Status m_status;
    // polje je aktivirano ako nije NORMAL_NOT_ACTIVE ili NORMAL_ACTIVE tj moguce im je
    // ,,nenasilno" menjati status
    bool m_isActivated;
};

#endif // FIELD_H
