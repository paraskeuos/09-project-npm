#ifndef SMASH_IT_H
#define SMASH_IT_H

#include "headers/games/game.h"
#include "headers/games/sibicarenje/notifications.h"
#include "headers/games/smash-it/field.h"
#include "headers/games/smash-it/smash_itbutton.h"

class Smash_It : public Game
{
    Q_OBJECT
public:
    Smash_It(QGraphicsView* parent, GameItem::GameId gameId);
    ~Smash_It();

public:
    bool getGameEnded() const;
    bool getHighScoreBeatenAnimation() const;

signals:
    // signal kojim se menja status polja na msec sekundi
    void changeStatus(Field* field, const Status &status, int msec);
    // svrha naredna dva signala objasnjena u field.h
    void changeStatusAlongTheDirection(Direction changeType, qint16 number, const Status &status, int msec);
    void forceChangeAlongTheDirection(Direction changeType, qint16 number, const Status &status, int msec);

public slots:
    void addPoints(qint32 points);
    void deductPoints(qint32 points);
    void endGame();
    void setGameMenu();

protected slots:
    void onConnectionError(QAbstractSocket::SocketError socketError) override;
    void connectedToServer() override;
    void disconnectedFromServer() override;
    void gameInit(int player, int gameIndex) override;
    void opponentMove(const QJsonObject &move) override;
    void onNewGameOffer() override;
    void onGameInterrupted() override;

private slots:
    void decreaseSecondsLeft();
    void decreaseSecondsLeft(qint32);
    void increaseSecondsLeft(qint32);
    void startGame();
    void restartGame();
    void eraseAllPoints();
    void returnToMenu();
    // promena bodova ili sekundi ce se oslikati desno pored poena igraca
    // potreban je slot koji ce taj tekst obrisati
    void removeClickEffectDisplay();
    void unavailableServer();

protected:
    void setGameScene() override;

private:
    void createRow(qreal, qreal, bool, qint16, qint16, qint16);
    // funkcija koja ce se pozivati sve vreme trajanja igre, ona ce
    // odabrati koja polja treba da menjaju status i pozvati funkciju
    // randomlyPickStatus za biranje statusa za ta polja
    void changeStatusOfFields();
    // funkcije addPoints i deductPoints zovu ovu funkciju za promenu broja bodova
    void setPoints(qint32);
    // postavljanje vrednosti za osnovne parametre igre
    void setDefaultGameParameters();
    // promena bodova ili sekundi tj efekat klika na polje ce se oslika desno
    // pored poena igraca
    void activateClickEffectDisplayTimer();
    // animacija koja se pokrece kada igrac obori highscore ili pobedi u multiplayer-u
    void highScoreBeatenAnimation();
    // funkcija koja sa razlicitim verovatnocama bira vraca neki od statusa
    Status randomlyPickStatus();
    void setInstructionsScene();

    // vektor polja koja cine teren
    QVector<Field*> m_fields;
    QTimer* m_timerChangeStatusOfFields, *m_timerDecreaseSecondsLeft;
    QTimer* m_timerClickEffectDisplay;
    // vektor koji sadrzi brojeve [0, m_fields.size), preko koga dohvatamo
    // polja iz vektora m_fields
    QVector<qint16>* m_rangeOfFieldIndexes;
    qint32 m_totalPoints, m_highScore;
    qint32 m_secondsLeft;
    QGraphicsTextItem* m_pointsDisplay, *m_secondsLeftDisplay, *m_clickEffectDisplay;
    QGraphicsTextItem* m_highScoreDisplay;
    Smash_ItButton* m_startGameButton;
    Smash_ItButton* m_singlePlayerButton, *m_multiplayerButton;
    Smash_ItButton* m_instructionsButton, *m_exitButton, *m_returnToMenuButton;
    bool m_gameEnded, m_gameStarted;

    // promenljive potrebne za funkciju highScoreBeatenAnimation
    QTimer* m_highScoreBeatenAnimationTimer;
    bool m_highScoreBeatenAnimation;
    qint16 m_antiDiagonalLimit, m_currentAntiDiagonal;
    bool m_currentSide;

    // promenljiva za obavestenja (kod multiplayer-a) kad igrac napusti igru, server padne i sl
    Notifications m_notification = Notifications();

    // promenljive potrebne za multiplayer varijantu igre
    // izgled scene dok cekamo drugog igraca
    void setWaitingScene();
    // izgled scene dok ne odaberemo da li hocemo novu igru posle kraja igre
    void setNewGameScene();
    void closeServerConnection();
    void showMessage(QString);
    // funkcija koja sluzi da promenimo poene protivnika, koristi se u da
    // bi igraci mogli da vide jedni drugima broj poena u realnom vremenu
    void setOpponentsPoints(qint32 points);

    bool m_multiplayerModeActive, m_opponentFinished;
    qint32 m_opponentsPoints;
    QGraphicsTextItem* m_opponentsPointsDisplay;
    bool m_winningAnimationStarted;
};

#endif // SMASH_IT_H
