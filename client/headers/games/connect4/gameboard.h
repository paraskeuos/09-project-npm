#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include "token.h"

#include <QGraphicsItem>
#include <QPainter>
#include <QTimer>

class GameBoard : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:
    GameBoard(const qreal boardWidth);

    // QGraphicsItem interface
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    QRectF rect() const;
    int rowNum() const;
    int colNum() const;

    QVector<QVector<Token *> >& tokens();

    qreal tokenStep() const;
    bool droppingToken() const;
    void droppingToken(bool droppingToken);

    qreal pendingStartX() const;
    void pendingStartX(const qreal &pendingX);

    qreal pendingStartY() const;
    void pendingStartY(const qreal &pendingY);

    qreal droppingEndY() const;
    void droppingEndY(const qreal &droppingEndY);

    int pickedCol() const;
    void pickedCol(int pickedCol);

    void moveHoverToken(int index);
    void dropToken(int index);
    Game::Player checkState();

signals:
    void pickedMove(int index);
    void droppedToken(int index);
    void checkValidHover(int index);

private slots:
    void onColumnHover(int index);
    void onColumnClick(int index);
    void animateTokenDrop();

private:
    bool isValidMove(int index);

private:
    QRectF m_rect;
    const int m_rowNum = 6;
    const int m_colNum = 7;
    bool m_droppingToken = false;

    QVector<QVector<Token*>> m_tokens;

    // Zeton nad tablom dok se bira potez
    qreal m_tokenStep;
    qreal m_pendingStartX;
    qreal m_pendingStartY;
    Token* m_pendingToken;

    // Podaci potrebni za animaciju pada zetona
    int m_pickedCol;
    qreal m_droppingEndY;
    QTimer* m_dropTimer;
};


#endif // GAMEBOARD_H
