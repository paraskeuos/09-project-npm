#ifndef GRIDBOT_H
#define GRIDBOT_H

#include "tilegrid.h"

#include <QThread>

class GridBot : public QThread
{
    Q_OBJECT
public:
    GridBot(const Game::Player& robotPlayerd, TileGrid* grid, QObject* parent = nullptr);

signals:
    void robotMadeMove();

    // QThread interface
protected:
    void run() override;

private:
    void playMove();
    void getBotBorderTiles(const int gridSize, QVector<int>& botTiles);
    void getTargetTiles(const int gridSize, QVector<int>& targets);
    void createAdjacencyList(const int gridSize, const QVector<int>& borderTiles);
    int findClosestTarget();
    void occupyTile(const int index);
    void mobilize();
    int closestTargetFloydWarshal(const int gridSize, const QVector<int>& borderTiles, const QVector<int>& targets);

    Game::Player botPlayer() const;
    int getGold() const;
    void setGold(int gold);

    int getArmies() const;
    void setArmies(int armies);
    int getMaxArmies() const;
    void setMaxArmies(int maxArmies);

private:
    Game::Player m_botPlayer;
    TileGrid* grid;
    int m_gold;
    int m_armies;
    int m_maxArmies;
    QVector<QVector<std::pair<int,int>>> m_adjacencyList;
};

#endif // GRIDBOT_H
