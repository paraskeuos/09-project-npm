#ifndef GRIDPOLITICS_H
#define GRIDPOLITICS_H

#include "gamebar.h"
#include "tilegrid.h"
#include "tilepanel.h"
#include <headers/games/game.h>
#include <QJsonArray>
#include <QPushButton>
#include <headers/games/paras_menuitem_lib/instructionspanel.h>
#include "gridbot.h"
#include "viewbutton.h"

class GridPolitics : public Game
{
    Q_OBJECT
public:
    GridPolitics(QGraphicsView* parent, GameItem::GameId gameId);
    ~GridPolitics();

    int gold() const;
    void setGold(int gold);
    int armies() const;
    void setArmies(int armies);
    int maxArmies() const;
    void setMaxArmies(int maxArmies);

    int pickedTile() const;
    void setPickedTile(int pickedTile);

    bool multiplayer() const;
    void setMultiplayer(bool multiplayer);

    ViewButton::ViewType pickedView() const;
    void setPickedView(const ViewButton::ViewType &pickedView);

public slots:
    void upgrade();
    void mobilize();
    void reinforce();
    void occupy();
    void onNextTurn();

protected slots:
    void onConnectionError(QAbstractSocket::SocketError socketError) override;
    void connectedToServer() override;
    void disconnectedFromServer() override;
    void opponentMove(const QJsonObject &move) override;
    void onNewGameOffer() override;
    void onGameInterrupted() override;
    void gameInit(int player, int gameIndex) override;

protected:
    void setGameScene() override;

private slots:
    void onTileSelected(const int i);
    void onChooseMode(bool multiplayer);
    void onRobotMove();
    void onChooseView(const ViewButton::ViewType& view);
    void onShowInstructions();
    void onCloseInstructions();

private:
    void setTilePanel();
    void removeTilePanel();
    void initGame();
    void setTilePanelButtons();
    void refreshTurn();
    void playOpponentMove(const QJsonValue& move);
    void setGameOverPanel(const Game::Player &winner);
    void setMainMenu();
    void setConnectScene(const QString& message, const bool isError);
    void sendGeneratedGrid();
    void applyGeneratedGrid(const QJsonValue& gridData);
    void setViewButtons();

    void playAgainSP();
    void noPlayAgainSP();
    void playAgainMP();
    void noPlayAgainMP();

private:
    bool m_multiplayer;
    bool m_gameOver;
    bool m_gridSet;
    TileGrid* m_grid;
    TilePanel* m_tilePanel;
    GridBot* m_robot;
    GameBar* m_gameBar;
    InstructionsPanel* m_instrPanel;
    int m_pickedTile;
    ViewButton::ViewType m_pickedView;
    int m_gold;
    int m_armies;
    int m_maxArmies;

    QPushButton* m_upgradeBtn;
    QPushButton* m_mobilizeBtn;
    QPushButton* m_reinforceBtn;
    QPushButton* m_occupyBtn;
    QPushButton* m_nextTurnBtn;

    QJsonArray m_actions;
};

#endif // GRIDPOLITICS_H
