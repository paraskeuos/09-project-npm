#ifndef GAMEBAR_H
#define GAMEBAR_H

#include <QGraphicsItem>
#include <QObject>
#include <QPainter>

class GameBar : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
public:
    GameBar(const qreal x, const qreal y, const qreal width, const qreal height);

    // QGraphicsItem interface
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

    int currGold() const;
    void setCurrGold(int currGold);

    int goldPerTurn() const;
    void setGoldPerTurn(int goldPerTurn);

    int currArmies() const;
    void setCurrArmies(int currArmies);

    int maxArmies() const;
    void setMaxArmies(int maxArmies);

private:
    QRectF m_rect;
    int m_currGold;
    int m_goldPerTurn;
    int m_currArmies;
    int m_maxArmies;
};

#endif // GAMEBAR_H
