#ifndef TESTEXITRECT_H
#define TESTEXITRECT_H

#include <QGraphicsItem>
#include <QPainter>
#include <QRectF>
#include <QStyleOptionGraphicsItem>
#include <QWidget>

class TestExitRect : public QGraphicsItem
{
public:
    TestExitRect();

    // QGraphicsItem interface
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    int type() const override;

private:
    QRectF m_rect = QRectF(-50, -50, 100, 100);
};

#endif // TESTEXITRECT_H
