#ifndef TESTGAME_H
#define TESTGAME_H

#include <headers/games/game.h>
#include <QGraphicsScene>

class TestGame : public Game
{
    Q_OBJECT
public:
    TestGame(QGraphicsView* parent, GameItem::GameId gameId);

    // Game interface
protected slots:
    void onConnectionError(QAbstractSocket::SocketError socketError) override;
    void connectedToServer() override;
    void disconnectedFromServer() override;
    void gameInit(int player, int gameIndex) override;
    void opponentMove(const QJsonObject &move) override;
    void onNewGameOffer() override;
    void onGameInterrupted() override;

protected:
    void setGameScene() override;
};

#endif // TESTGAME_H
