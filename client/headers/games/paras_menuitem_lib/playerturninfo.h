#ifndef PLAYERTURNINFO_H
#define PLAYERTURNINFO_H

#include <QGraphicsItem>
#include <QPainter>

class PlayerTurnInfo : public QGraphicsItem
{
public:
    PlayerTurnInfo(qreal x, qreal y, qreal width, qreal height);

    // QGraphicsItem interface
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

private:
    QRectF m_rect;
};

#endif // PLAYERTURNINFO_H
