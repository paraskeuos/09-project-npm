#ifndef MESSAGEFIELD_H
#define MESSAGEFIELD_H

#include <QGraphicsItem>
#include <QPainter>
#include <QPen>

class MessageField : public QGraphicsItem
{
public:
    MessageField(const qreal x, const qreal y,
                 const qreal width, const qreal height,
                 const QString&, const bool isError);

    // QGraphicsItem interface
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

private:
    QRectF m_rect;
    QString m_message;
    bool m_isError;


};

#endif // MESSAGEFIELD_H
