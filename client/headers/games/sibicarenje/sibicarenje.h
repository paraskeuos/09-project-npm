#ifndef SIBICARENJE_H
#define SIBICARENJE_H

#include "headers/games/game.h"
#include "headers/games/sibicarenje/box.h"
#include "headers/games/sibicarenje/ball.h"
#include "headers/games/sibicarenje/currentbox.h"
#include "headers/games/sibicarenje/sibicarenjebutton.h"
#include "headers/games/sibicarenje/sibicarenjebackground.h"
#include "headers/games/sibicarenje/notifications.h"

#include <QObject>
#include <QLCDNumber>
#include <QPushButton>

class Sibicarenje : public Game
{
    Q_OBJECT
public:
    Sibicarenje(QGraphicsView* parent, GameItem::GameId gameId);

signals:
    // signal koji se salje svim kutijama sa informacijom na koju kutiju se odnosi,
    // na koju stranu treba da se izvrsi rotacija i koji je trenutni nivo (zarad brzine)
    void rotateBox(CurrentBox, qint16 side, qint16 level);
    // signal koji sluzi da iz ove klase blokiramo kutije
    void blockOpeningBox(bool);

public slots:
    // funkcija koja zapravo bira jednu od pet glavnih rotacija kutija
    void chooseNextRotation(bool);
    // funkcija koja priprema narednu rotaciju
    void prepareNewRotation();
    // funkcija koja proverava da li je igrac pogodio kutiju u kojoj se nalazi loptica
    void checkPlayerChoice(CurrentBox);
    void showBall();
    void hideBall();
    void setGameMenu();

private slots:
    void increaseLCD();
    void returnToMenu();
    void unavailableServer();
    void closeServerConnection();

    // Game interface
protected slots:
    void onConnectionError(QAbstractSocket::SocketError socketError) override;
    void connectedToServer() override;
    void disconnectedFromServer() override;
    void gameInit(int player, int gameIndex) override;
    void opponentMove(const QJsonObject &move) override;
    void onNewGameOffer() override;
    void onGameInterrupted() override;

protected:
    void setGameScene() override;

private:
    void setWaitingScene();
    void setNewGameScene();
    void setInstructionsScene();
    void showMessage(QString);
    void setBackground();

    Box *m_box1, *m_box2, *m_box3;
    Ball *m_ball;
    QLCDNumber *m_levelLCD;
    QPushButton *m_buttonStartRotations;
    SibicarenjeButton *m_returnToMenuButton, *m_exitButton;

    // currentRotationNumber sluzi da odredimo koja je rotacija trenutno aktivna
    //  u seriji od  numberOfRotations rotacija
    qint16 m_currentRotationNumber, m_numberOfRotations;
    // ne zelimo da imamo dve uzastopne iste rotacije pa cuvamo informaciju o
    // prethodnoj vrednosti za rotaciju
    quint32 m_previousRotationValue;

    QGraphicsTextItem *m_hitMsg, *m_missMsg, *m_gameOverMsg, *m_endGameMsg;
    qint16 m_level;

    SibicarenjeButton *m_singlePlayerButton, *m_multiplayerButton, *m_instructionsButton;
    bool m_restartGame = false;
    bool m_gameFinished = false;

    // promenljiva koja ce sluziti za ispis poruka kad smo diskonektovani sa servera
    // i za konacan rezultat multiplayer igre
    Notifications m_notification = Notifications();

    // multiplayer deo
    qint16 m_opponentLevel = -1;
    bool m_multiplayerModeActive = false;
};

#endif // SIBICARENJE_H
