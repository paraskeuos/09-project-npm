#include "headers/view.h"
#include <headers/dashboard/gameitem.h>
#include <headers/dashboard/gamelist.h>

#include <QPainter>
#include <QDebug>
#include <QGraphicsSceneWheelEvent>

#include <headers/games/testgame/testgame.h>

#include <headers/games/connect4/connect4.h>
#include <headers/games/sibicarenje/sibicarenje.h>


#include <headers/games/gridpolitics/gridpolitics.h>
#include <headers/games/smash-it/smash_it.h>

View::View()
    : QGraphicsView()
{
    setMouseTracking(true);
    setRenderHint(QPainter::Antialiasing);
    setFixedSize(m_width, m_height);
    setScene(new QGraphicsScene);

    //m_game ce biti nullptr kada god se nalazimo u glavnom meniju
    m_game = nullptr;
    // Pri pokretanju aplikacije se postavlja pocetna scena
    // sa listom igara u samom konstruktoru Dashboard klase
    m_dashboard = std::make_unique<Dashboard>(
                Dashboard(m_width, m_height, m_background, scene()));
}

View::~View()
{
    delete scene();
}

/**
 * @brief View::mousePressEvent
 * @param event
 *
 * Obradjuju se klikovi na GameItem objekte za
 * izbor igre kao i klikovi na objekte unutar
 * igara za izlas iz istih u povratak na dashboard
 * scenu.
 *
 * Svaka igra mora da prevazidje type() funkciju,
 * vrati svoju Game::GameID vrednost i prosledi
 * klik dogadjaj pogledu (ne pozivati event->accept()).
 *
 */
void View::mousePressEvent(QMouseEvent *event)
{
    QGraphicsView::mousePressEvent(event);
    if(event->isAccepted()) {
        return;
    }

    QGraphicsItem* item = itemAt(event->pos());
    if(item == nullptr) {
        return;
    }

    switch(item->type()) {
    // Na dashboard sceni je izabrana igra za pokretanje
    case GameItem::StartGame: {

        GameItem* gameItem = dynamic_cast<GameItem*>(item);
        if(!gameItem) {
            return;
        }

        switch(gameItem->gameId()) {
        case GameItem::Connect4: {
            m_game = new Connect4(this, gameItem->gameId());
            break;
        }
        case GameItem::Sibicarenje: {
            m_game = new Sibicarenje(this, gameItem->gameId());
            break;
        }
        case GameItem::Game3:
        case GameItem::GridPolitics: {
            m_game = new GridPolitics(this, gameItem->gameId());
            break;
        }
        case GameItem::Smash_It:{
            m_game = new Smash_It(this, gameItem->gameId());
            break;
        }
        case GameItem::Game6: {

            m_game = new TestGame(this, gameItem->gameId());
            break;
        }
        default:
            break;
        }

        event->accept();
        break;
    }
    // Korisnik zeli da prekinu igru, vracanje na dashboard scenu
    case Game::ExitGame: {

        event->accept();
        m_game->deleteLater();
        // mora da se postavi na nullptr da bi mogle da se koriste strelice u glavnom meniju
        m_game = nullptr;
        m_dashboard->setDashboard();
        break;
    }
    default:
        break;
    }
}

void View::keyPressEvent(QKeyEvent *event)
{
    QGraphicsView::keyPressEvent(event);
    /*if(event->isAccepted() == true) {
        return;
    }*/

    //u ovaj blok se ulazi samo ako smo u glavnom meniju
    if(m_game==nullptr){

        if(event->key() == Qt::Key_Escape){
            close();
        }

        GameList* gameList = nullptr;
        // prvo trazimo koji element scene predstavlja nas GameList* objekat
        for(auto &el : scene()->items()){
            if(dynamic_cast<GameList*>(el) != nullptr){
                gameList = dynamic_cast<GameList*>(el);
                break;
            }
        }

        // ako je razlicit od nullptr, tj ako smo ga pronasli (sto bi uvek
        // trebalo da vazi kada smo u glavnom meniju
        if(gameList!=nullptr){
            int i = 0;
            // zastavica koja govori da li je neka slicica igre uvecana tj odabrana
            bool anyGameItemExpanded = false;
            for(QGraphicsItem* temp: gameList->childItems()){
                GameItem* gameItem = (GameItem*)temp;
                if(gameItem->isExpanded()){
                    // zastavica se postavlja na tacno ako je polje prosireno
                    anyGameItemExpanded = true;
                    // ako je polje prosireno i pritisnut je taster Enter, treba pokrenuti igru
                    if(event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return){
                        /*    ovde je iskoriscena vec implementirana funkcionalnost za pokretanje igre
                                preko misa, jedini vazan argument u konstruktoru QMouseEvent je
                                gameItem->scenePos() jer se time odredjuje koja je igra u pitanju,
                                ostali argumenti su proizvoljno postavljeni i nebitni ali neophodni
                                da bi dogadjaj mogao da se emituje  */
                        QMouseEvent* tempEvent = new QMouseEvent(
                                    QEvent::None, gameItem->scenePos(), Qt::NoButton, Qt::NoButton, Qt::NoModifier);
                        emit mousePressEvent(tempEvent);
                        break;
                    }
                }

                if(event->key() == Qt::Key_Right){
                    /* ako je pritisnuta strelica na desno, trebalo bi izvrsiti animaciju
                        koja pomera trenutno odabranu igru
                        callWheel poziva animaciju za pomeranje svih slicica i to zelimo samo
                        u slucaju da je trenutni argument uvecan i ako je drugi ili kasniji u
                        nizu slicica  */
                    if(i>=2 && gameItem->isExpanded()){
                        gameList->callWheel(true);
                    }
                    // preko metoda moveGameIcon umanjujemo prethodno odabranu slicicu
                    // i uvecavamo sledecu
                    gameItem->moveGameIcon(true);

                }
                else if(event->key() == Qt::Key_Left){
                    // slicno kao za taster desno, samo je granica za i drugacija,
                    // kao i smer kretanja
                    if(i<=3 && gameItem->isExpanded())
                        gameList->callWheel(false);
                    gameItem->moveGameIcon(false);
                }
                i++;
            }

            // ako nijedno polje nije prosireno tj nismo oznacili nijednu igru
            // a pritisnuto je levo ili desno dugme na tastaturi, automatski se bira
            // slicica prve igre kao prosirena
            if(anyGameItemExpanded == false && (event->key() == Qt::Key_Right || event->key() == Qt::Key_Left)){
                gameList->resetGameListPosition();
                GameItem* firstGameItem = (GameItem*)gameList->childItems()[0];
                firstGameItem->setToExpand(true);
                firstGameItem->moveGameIcon(true);
            }

        }
    }


    event->accept();
}
