#include <headers/dashboard/dashboard.h>
#include <headers/dashboard/gamelist.h>

#include <QDebug>

#include <QPen>
#include <QPixmap>

// Dimenzije scene moraju biti malo manje od dimenzija pogleda/prozora
// koje su fiksne, kako se ne bi pojavljivali scrollbarovi sa strana
Dashboard::Dashboard(int width, int height, QImage background, QGraphicsScene *scene)
    : m_width(width - 5), m_height(height - 5), m_scene(scene), m_background(background)
{
    setDashboard();
}

/**
 * @brief Dashboard::setDashboard
 *
 * Postavlja scenu za dashboard (lista za izbor igara)
 * pri pokretanju aplikacije i pri izlazu iz igara.
 */
void Dashboard::setDashboard() const
{
    // Ukoliko je korisnik prethodno igrao neku igru, cisti se scena
    m_scene->clear();
    m_scene->setSceneRect(0, 0, m_width, m_height);
    m_scene->setBackgroundBrush(m_background);

    auto text = m_scene->addText("Choose a game");
    text->setScale(3);
    text->moveBy( m_scene->sceneRect().height()/2,
                  m_scene->sceneRect().width()/10 );

    // Privremeno - koordinatni sistem
//    m_scene->addLine(-m_width/4, 0, 5*m_width/4, 0, QPen(Qt::white));
//    m_scene->addLine(-m_gameItemSize - m_gameItemSize/8, -m_height/2,
//                     -m_gameItemSize - m_gameItemSize/8, m_height/2, QPen(Qt::white));

    GameList* gameList = new GameList(m_gameItemSize, m_gameNum, m_width, m_height);
    m_scene->addItem(gameList);
}
