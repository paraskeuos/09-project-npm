#include <headers/dashboard/gameitem.h>
#include <QDebug>
#include <QGraphicsScene>

GameItem::GameItem(GameId gameId, qreal size, QImage image, GameDescriptionBox* gameDescription)
{
    m_gameId = gameId;
    m_size = size;
    m_img = image;
    m_rect = QRectF(-m_size/2, -m_size/2, m_size, m_size);

    // Podesavanje min. i maks. velicine kvadrata prilikom animacija
    m_minRect = m_rect;
    m_maxRect = QRectF(m_rect.x() - m_size/4, m_rect.y() - m_size/4,
                       m_rect.width() + m_size/2, m_rect.height() + m_size/2);

    m_descriptionBox = gameDescription;

    // na pocetku se zastavice koje odredjuju da li je polje prosireno ili ga
    // treba prosiriti stavljaju na false
    setIsExpanded(false);
    setToExpand(false);

    setEnabled(true);
    setAcceptHoverEvents(true);
}

GameItem::~GameItem()
{
//    GameItem::descriptionBox = false;
}

GameItem::GameId GameItem::gameId() const
{
    return m_gameId;
}

void GameItem::setRect(QRectF& rect)
{
    if(m_rect == rect) {
        return;
    }

    prepareGeometryChange();
    m_rect = rect;
}

QRectF GameItem::boundingRect() const
{
    return m_rect;
}

void GameItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->drawImage(m_rect, m_img);

    Q_UNUSED(option);
    Q_UNUSED(widget);
}

int GameItem::type() const
{
    return StartGame;
}

void GameItem::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    QGraphicsItem::hoverEnterEvent(event);
    if(m_rect == m_maxRect) {
        return;
    }

    // Ako je tajmer aktivan u toku je animacija u suprotnom "smeru",
    // treba je prekinuti i pokrenuti novu
    if(m_timer) {
        m_timer->stop();

        m_timer->deleteLater();
        m_timer = nullptr;
    }

    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(expand()));

    m_timer->start(1000/100);

    // ako je pokrenut hoverEnterEvent u trenutku kada je vec neka druga
    // slicica uvecana, treba je smanjiti tj poslati joj signal da to uradi
    for (auto temp: parentItem()->childItems()){
        GameItem* temp2 = (GameItem*)temp;
        if(temp2->isExpanded()){
            temp2->setIsExpanded(false);
            temp2->hoverLeaveEvent(new QGraphicsSceneHoverEvent);
        }
    }

    // nakon zavrsetka prosirenja polja treba postaviti zastavicu da je polje prosireno
    setIsExpanded(true);

    event->accept();

}

void GameItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    QGraphicsItem::hoverLeaveEvent(event);
    if(m_rect == m_minRect) {
        return;
    }

    // Ako je tajmer aktivan u toku je animacija u suprotnom "smeru",
    // treba je prekinuti i pokrenuti novu
    if(m_timer) {
        m_timer->stop();

        m_timer->deleteLater();
        m_timer = nullptr;
    }

    m_timer = new QTimer(this);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(shrink()));

    m_timer->start(1000/100);

    // nakon zavrsetka smanjenja polja treba postaviti zastavicu da je polje smanjeno tj
    // da nije prosireno
    setIsExpanded(false);

    event->accept();

}

void GameItem::showGameDescription()
{
    switch(gameId()){
        case GameItem::Connect4:
            m_descriptionBox->setGameName("Connect4");
            break;
        case GameItem::Sibicarenje:
            m_descriptionBox->setGameName("Sibicarenje");
            break;
        case GameItem::Game3:
            m_descriptionBox->setGameName("");
            break;
        case GameItem::GridPolitics:
            m_descriptionBox->setGameName("GridPolitics");
            break;
        case GameItem::Smash_It:
            m_descriptionBox->setGameName("Smash-It");
            break;
        case GameItem::Game6:
        default:
            m_descriptionBox->setGameName("");
            break;
    }
    scene()->addItem(m_descriptionBox);
}

void GameItem::hideGameDescription()
{
    if(m_rect == m_maxRect){
        if(m_descriptionBox->scene()!=nullptr)
            scene()->removeItem(m_descriptionBox);
    }
}
//true - desno, false - levo
void GameItem::moveGameIcon(bool side)
{
    // ako smo na trenutnoj igri koja je prva i vec odabrana,
    // a pritisnut taster za levo, ne zelimo da se ista desava
    if(gameId() == GameItem::Connect4 && side == false && isExpanded())
        return;

    // ako smo na trenutnoj igri koja je poslednja i vec odabrana,
    // a pritisnut taster za desno, ne zelimo da se ista desava
    if(gameId() == GameItem::Game6 && side && isExpanded())
        return;

    // ako trenutnu slicicu treba prosiriti, salje se odgovaraju signal
    // i postavlja odgovarajuca zastavica
    if(toExpand()){
        emit hoverEnterEvent(new QGraphicsSceneHoverEvent());
        setToExpand(false);
        return;
    }

    // ako nismo u granicnim slucajevima (prva ili poslednja igra), niti trenutnu
    // slicicu sad treba prosiriti, a ako je bila prosirena, onda je treba smanjiti
    // i postaviti levu ili desnu slicicu (zavisno od tastera) da se uveca
    if(isExpanded()){
        emit hoverLeaveEvent(new QGraphicsSceneHoverEvent());
        int value = 0;
        // ako je side true tj desni taster je pritisnut, onda treba uzeti narednu igru (gameId() + 1)
        // medjutim, ako je side false, tj levi taster je pritisnut, onda treba uzeti prethodnu igru
        // (gameId() - 1)
        // razlika izmedju +1 i -1 je 2, i zato ce se value postaviti na -2 i sabrati u indeksiranju kasnije
        // ako je u pitanju levi taster
        if(side!=true){
            value=-2;
        }
        GameItem* temp = dynamic_cast<GameItem*>((this->parentItem()->
                                                  childItems()[ gameId()+value+1 ]));
        // postavi odabranu igru kao igru koju treba prosiriti
        temp->setToExpand(true);
        // ako je value negativno, to znaci da smo vec prosli slicicu koju treba da prosirimo
        // (jer uvek idemo s leva nadesno), pa treba napraviti izuzetak i vanredno pozvati
        // funkciju moveGameIcon za odabrani GameItem*
        if(value<0)
            temp->moveGameIcon(side);
    }
}

bool GameItem::isExpanded()
{
    return m_isExpanded;
}

void GameItem::setIsExpanded(bool flag)
{
    m_isExpanded = flag;
}

bool GameItem::toExpand()
{
    return m_toExpand;
}

void GameItem::setToExpand(bool flag)
{
    m_toExpand = flag;
}

void GameItem::wheelEvent(QGraphicsSceneWheelEvent *event)
{
    QGraphicsItem::wheelEvent(event);
//    scene()->removeItem(m_descriptionBox);
    hideGameDescription();
}

/**
 * @brief GameItem::expand
 *
 * Poziva se preko tajmera za animaciju povecanja slike
 */
void GameItem::expand()
{
    const qreal step = 2;
    QRectF rect = QRectF(m_rect.x() - step, m_rect.y() - step,
                         m_rect.width() + 2*step, m_rect.height() + 2*step);
    setRect(rect);

    if(m_rect == m_maxRect) {

        m_timer->stop();

        m_timer->deleteLater();
        m_timer = nullptr;

        // kutiju sa opisom treba napraviti tek kada se slicica do kraja prosiri
        showGameDescription();

    }

    }

/**
 * @brief GameItem::shrink
 * Poziva se preko tajmera za animaciju smanjenja slike
 */
void GameItem::shrink()
{
    hideGameDescription();
//    scene()->removeItem(m_descriptionBox);

    const qreal step = 2;
    QRectF rect = QRectF(m_rect.x() + step, m_rect.y() + step,
                         m_rect.width() - 2*step, m_rect.height() - 2*step);
    setRect(rect);

    if(m_rect == m_minRect) {

        m_timer->stop();

        m_timer->deleteLater();
        m_timer = nullptr;
    }
}
