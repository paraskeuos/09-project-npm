#include "headers/dashboard/gamedescriptionbox.h"

#include <QGraphicsScene>
#include <QPainter>
#include <QDebug>

GameDescriptionBox::GameDescriptionBox(QRectF rect):
    m_rect(rect)
{
    m_gameName = "";
}

QRectF GameDescriptionBox::boundingRect() const
{
    return QRectF(m_rect.bottomLeft().x()+340, m_rect.bottomLeft().y()+300,
                  m_rect.width()*2, m_rect.height()/2);
}

void GameDescriptionBox::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(QPen(QColor(0, 0, 0), 1));
    painter->setBrush(Qt::white);

    QFont font = painter->font() ;
    font.setPointSize(font.pointSize() * 1.5);
    font.setBold(true);
    painter->setFont(font);

    painter->drawText(boundingRect(), Qt::AlignCenter, m_gameName);

    Q_UNUSED(option);
    Q_UNUSED(widget);
}

void GameDescriptionBox::setGameName(QString gameName)
{
    m_gameName = gameName;
}
