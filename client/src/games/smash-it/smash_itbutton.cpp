#include "headers/games/smash-it/smash_itbutton.h"

#include <headers/games/game.h>

Smash_ItButton::Smash_ItButton(qint16 x, qint16 y, qint16 width, qint16 height, QString text):
    m_text(text), m_x(x), m_y(y), m_width(width), m_height(height)
{
    m_rect = QRectF(x, y, width, height);
}

QRectF Smash_ItButton::boundingRect() const
{
    return m_rect;
}

void Smash_ItButton::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QColor color;
    color.setRgb(239,239,239);
    painter->setBrush(color);

    painter->drawEllipse(boundingRect());
    painter->drawText(boundingRect(), Qt::AlignCenter, m_text);

    Q_UNUSED(option);
    Q_UNUSED(widget);
}

int Smash_ItButton::type() const
{
    return Game::ExitGame;
}

void Smash_ItButton::setText(QString &&text)
{
    m_text = text;
    update();
}

void Smash_ItButton::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    if(m_text.compare("single player") == 0)
        emit singlePlayerMode();
    else if(m_text.compare("multiplayer") == 0 || m_text.compare("yes") == 0)
        emit multiPlayerMode();
    else if(m_text.compare("start game") == 0)
        emit startGame();
    else if(m_text.compare("restart") == 0)
        emit restartGame();
    else if(m_text.compare("menu") == 0 || m_text.compare("no") == 0 || m_text.compare("back") == 0)
        emit returnToMenu();
    else if(m_text.compare("instructions") == 0)
        emit instructions();
    else if(m_text.compare("exit")==0){
        emit endGame();
        // ne zelimo da prihvatamo dogadjaj, tj zelimo da view odradi svoje
        return;
    }
    event->accept();
}
