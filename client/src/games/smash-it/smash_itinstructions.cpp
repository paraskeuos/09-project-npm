#include "headers/games/smash-it/smash_itinstructions.h"

#include <QGraphicsSceneMouseEvent>
#include <QPainter>

Smash_ItInstructions::Smash_ItInstructions()
{
    setAcceptHoverEvents(true);
}

QRectF Smash_ItInstructions::boundingRect() const
{
    return QRectF(-400,-320,850,650);
}

void Smash_ItInstructions::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QFont font = painter->font() ;
    font.setPointSize(font.pointSize() * 1.5);
    painter->setFont(font);

    painter->drawText(QRectF(150, 125, 300, 200), "The goal is simple - collect as much points "
                                                  "as possible in 60 seconds.\n"
                                                  "But be careful - there are fields that can penalize you - you\n"
                                                  "can lose 10 points, 10 seconds, or even all of your points!");
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

void Smash_ItInstructions::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    event->accept();
}

void Smash_ItInstructions::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    event->accept();
}

void Smash_ItInstructions::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    event->accept();
}
