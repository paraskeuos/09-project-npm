#include "headers/games/smash-it/field.h"
#include "headers/games/smash-it/smash_it.h"

#include <QPainter>
#include <QPolygonF>
#include <QGraphicsSceneHoverEvent>
#include <QTimer>

Field::Field(qint16 antiDiagonalNumber, qint16 mainDiagonalNumber,
             qint16 rowNumber, qint16 columnNumber, Status status):
    m_antiDiagonalNumber(antiDiagonalNumber), m_mainDiagonalNumber(mainDiagonalNumber),
    m_rowNumber(rowNumber), m_columnNumber(columnNumber)
{
    setAcceptHoverEvents(true);
    setStatus(status);
    m_timer = nullptr;
}

QRectF Field::boundingRect() const
{
    return QRectF(-32.5, -35, 32.5*2, 35*2);
}

void Field::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    auto field = QPolygonF( QVector<QPointF> {QPointF(0,-35), QPointF(-32.5,-20), QPointF(-32.5, 20),
                                          QPointF(0,35), QPointF(32.5, 20), QPointF(32.5,-20)});
    painter->setPen(QPen(Qt::white));
    painter->setBrush(m_color);
    painter->drawPolygon(field);

    painter->setPen(QPen(m_color));
    painter->setBrush(Qt::white);
    QFont font = painter->font() ;
    font.setPointSize(font.pointSize() * 3);
    font.setBold(true);
    painter->setFont(font);

    // u zavisnosti od statusa polja iscrtavaju se razlicite stvari
    // u prvom switch-u obradjena samo dva slucajeva jer su oni jedini gde se ne
    // iscrtava elipsa
    switch(m_status){

        case MINUS_10_POINTS_PRESSED: {
            painter->setPen(Qt::NoPen);

            auto xFirstPart = QPolygonF(QVector<QPointF>{ QPointF(-12.5,-17.5), QPointF(-17.5,-12.5),
                                             QPointF(12.5,17.5), QPointF(17.5,12.5)});
            auto xSecondPart = QPolygonF(QVector<QPointF>{ QPointF(-17.5, 12.5), QPointF(-12.5,17.5),
                                                 QPointF(17.5,-12.5), QPointF(12.5,-17.5)});
            painter->drawPolygon(xFirstPart);
            painter->drawPolygon(xSecondPart);
            return;
        }
        case PLUS_5_POINTS_PRESSED:{
            auto checkmark = QPolygonF(QVector<QPointF>{
                              QPointF(-6,3), QPointF(-13.5,-6), QPointF(-18.5,-1),
                              QPointF(-4.75,12.75), QPointF(19,-11), QPointF(14,-16)});
            painter->drawPolygon(checkmark);
            return;
        }
        default:
            break;
    }

    painter->drawEllipse(-20, -20, 40, 40);
    switch(m_status){
        case DISABLE_ANTI_DIAGONAL:
            painter->drawText(-18,14,"⤢");
            break;
        case DISABLE_MAIN_DIAGONAL:
            painter->drawText(-18,14,"⤡");
            break;
        case DISABLE_ROW:
            painter->drawText(-18,14,"↔");
            break;
        case DISABLE_COLUMN:
            painter->drawText(-18,14,"↕");
            break;
        case PLUS_5_SECONDS:
            font.setPointSize(font.pointSize() / 3);
            painter->setFont(font);
            painter->drawText(-18,5, " +5s");
            break;
        case MINUS_10_SECONDS:
            font.setPointSize(font.pointSize() / 3);
            painter->setFont(font);
            painter->drawText(-16,5, "-10s");
            break;
        case ZERO_POINTS:
            font.setPointSize(font.pointSize()*2/3);
            painter->setFont(font);
            painter->drawText(-8,10,"0");
            break;
        default:
            break;
    }

    Q_UNUSED(option);
    Q_UNUSED(widget);
}

void Field::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    QGraphicsItem::hoverEnterEvent(event);
    event->accept();

    switch (status()) {
        case MINUS_10_POINTS_PRESSED:
        case MINUS_10_POINTS_NOT_PRESSED:
        case MINUS_10_SECONDS:
            m_color=LIGHT_CRIMSON;
            return;
        case PLUS_5_POINTS_PRESSED:
        case PLUS_5_POINTS_NOT_PRESSED:
        case PLUS_5_SECONDS:
            m_color = LIGHT_LIME_GREEN;
            return;
        case NO_EFFECT:
            m_color = DARK_GRAY;
            return;
        case NORMAL_NOT_ACTIVE:
            setStatus(NORMAL_ACTIVE);
        default:
            break;
    }
}

void Field::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    QGraphicsItem::hoverEnterEvent(event);
    event->accept();

    switch (status()) {
        case MINUS_10_POINTS_PRESSED:
        case MINUS_10_POINTS_NOT_PRESSED:
        case MINUS_10_SECONDS:
            m_color=CRIMSON;
            return;
        case PLUS_5_POINTS_PRESSED:
        case PLUS_5_POINTS_NOT_PRESSED:
        case PLUS_5_SECONDS:
            m_color = LIME_GREEN;
            return;
        case NO_EFFECT:
            m_color = GRAY;
            return;
        case NORMAL_ACTIVE:
            setStatus(NORMAL_NOT_ACTIVE);
        default:
            break;
    }
}

void Field::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    event->accept();

    if(gameEnded()==true)
        return;

    // u zavisnosti od statusa, pritiskom na dugme se izvrsava drugacija akcija
    // tj emituje se drugaciji signal
    switch (status()) {
        case MINUS_10_POINTS_NOT_PRESSED:
            setStatus(MINUS_10_POINTS_PRESSED);
            emit deductPoints(10);
            break;
        case PLUS_5_POINTS_NOT_PRESSED:
            setStatus(PLUS_5_POINTS_PRESSED);
            emit addPoints(5);
            break;
        case DISABLE_ROW:
            emit changeStatusAlongTheDirection(ROW, m_rowNumber, NO_EFFECT, 2500);
            emit addPoints(10);
            break;
        case DISABLE_COLUMN:
            emit changeStatusAlongTheDirection(COLUMN, m_columnNumber, NO_EFFECT, 2500);
            emit addPoints(10);
            break;
        case DISABLE_MAIN_DIAGONAL:
            emit changeStatusAlongTheDirection(MAIN_DIAGONAL, m_mainDiagonalNumber, NO_EFFECT, 2500);
            emit addPoints(10);
            break;
        case DISABLE_ANTI_DIAGONAL:
            emit changeStatusAlongTheDirection(ANTI_DIAGONAL, m_antiDiagonalNumber, NO_EFFECT, 2500);
            emit addPoints(10);
            break;
        case PLUS_5_SECONDS:
            emit increaseSecondsLeft(5);
            backToNormalNotActiveStatus();
            break;
        case MINUS_10_SECONDS:
            emit decreaseSecondsLeft(10);
            backToNormalNotActiveStatus();
            break;
        case ZERO_POINTS:
            emit eraseAllPoints();
            backToNormalNotActiveStatus();
            break;
        default:
            return;
    }
    this->update();
}

bool Field::gameEnded() const
{
    return static_cast<Smash_It*>(parent())->getGameEnded();
}

bool Field::highScoreBeatenAnimationActive() const
{
    return static_cast<Smash_It*>(parent())->getHighScoreBeatenAnimation();
}

Status Field::status() const
{
    return m_status;
}

void Field::setStatus(const Status &status)
{
    m_status = status;

    // prilikom menjanja statusa menja se i boja
    switch(m_status){
        case NO_EFFECT:
            m_color = GRAY;
            break;
        case NORMAL_NOT_ACTIVE:
            m_color = DARK_GOLD;
            break;
        case NORMAL_ACTIVE:
            m_color = LIGHT_GOLD;
            break;
        case MINUS_10_POINTS_PRESSED:
        case MINUS_10_POINTS_NOT_PRESSED:
        case MINUS_10_SECONDS:
            m_color = CRIMSON;
            break;
        case PLUS_5_POINTS_PRESSED:
        case PLUS_5_POINTS_NOT_PRESSED:
        case PLUS_5_SECONDS:
            m_color = LIME_GREEN;
            break;
        case DISABLE_ROW:
            m_color = DISABLE_ROW_COLOR;
            break;
        case DISABLE_COLUMN:
            m_color = DISABLE_COLUMN_COLOR;
            break;
        case DISABLE_MAIN_DIAGONAL:
            m_color = DISABLE_MAIN_DIAGONAL_COLOR;
            break;
        case DISABLE_ANTI_DIAGONAL:
            m_color = DISABLE_ANTI_DIAGONAL_COLOR;
            break;
        case ZERO_POINTS:
            m_color = BLACK;
            break;
        default:
            break;
    }

    // potrebno je da promenimo zastavicu m_isActivated kako polje
    // ne bi moglo da menja status iz spoljne klase ako je aktivirano
    if(m_status==NORMAL_ACTIVE || m_status==NORMAL_NOT_ACTIVE)
        m_isActivated = false;
    else
        m_isActivated = true;

    // zahtevamo da polje promeni i izgled, tj pozivamo funkciju paint()
    update();
}

// sluzi za spoljno menjanje statusa (ovo je slot)
void Field::changeStatus(Field* field, const Status &status, int msec)
{
    // sprecavamo da se spolja menja status polja ako je igra zavrsena, a
    // nije u toku animacija za obaranje highscore-a (koja se koristi i u
    // multipalyer-u kod pobednika)
    if(gameEnded()==true && highScoreBeatenAnimationActive() == false){
        backToNormalNotActiveStatus();
        return;
    }

    // posto se signal prosledjuje svim poljima, ne zelimo da menjamo status
    // ako u pitanju nije konkretno polje
    if(field != this)
        return;

    // ako je polje aktivirano, ne zelimo da mu menjamo status
    if(m_isActivated == true)
        return;

    setStatus(status);

    // pravimo tajmer, kada istekne treba vratiti polje u normalan status
    m_timer = new QTimer();
    connect(m_timer, &QTimer::timeout, this, &Field::backToNormalNotActiveStatus);
    m_timer->setSingleShot(true);
    m_timer->start(msec);
}

void Field::onForceChangeAlongTheDirection(Direction changeType, qint16 number, const Status &status, int msec)
{
    switch (changeType){
        case ANTI_DIAGONAL:
            // ako se menjaju sva polja duz sporedne dijagonale, zelimo samo
            // da se promene ona koja imaju isti broj sporedne dijagonale
            // analogno za ostale slucajeve
            if(m_antiDiagonalNumber!=number)
                return;
            break;
        case MAIN_DIAGONAL:
            if(m_mainDiagonalNumber!=number)
                return;
            break;
        case ROW:
            if(m_rowNumber!=number)
                return;
            break;
        case COLUMN:
            if(m_columnNumber!=number)
                return;
            break;
    }

    // ako je tajmer bio aktivan, potrebno je da ga zaustavimo i obrisemo
    // da nam ne bi vratio polje na NORMAL_NOT_ACTIVE pre nego sto zelimo
    if(m_timer!=nullptr && m_timer->isActive()==true){
        m_timer->stop();
        delete m_timer;
        m_timer=nullptr;
    }

    m_isActivated = false;
    changeStatus(this, status, msec);
}

void Field::backToNormalNotActiveStatus()
{
    if(m_timer!=nullptr){
        delete m_timer;
        m_timer=nullptr;
    }

    setStatus(NORMAL_NOT_ACTIVE);
}
