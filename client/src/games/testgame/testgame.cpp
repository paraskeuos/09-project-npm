#include <headers/games/testgame/testexitrect.h>
#include <headers/games/testgame/testgame.h>

#include <QDebug>

TestGame::TestGame(QGraphicsView* parent, GameItem::GameId gameId)
    : Game(parent, gameId)
{
    setGameScene();

    const GameClient* client = gameClient();
    connect(client, &GameClient::connected, this, &TestGame::connectedToServer);
    connect(client, &GameClient::disconnected, this, &TestGame::disconnectedFromServer);
    connect(client, &GameClient::error, this, &TestGame::onConnectionError);
    connect(client, &GameClient::gameInit, this, &TestGame::gameInit);
    connect(client, &GameClient::opponentMove, this, &TestGame::opponentMove);
    connect(client, &GameClient::newGameOffer, this, &TestGame::onNewGameOffer);
    connect(client, &GameClient::gameInterrupted, this, &TestGame::onGameInterrupted);

    // Povezivanje na server
    connectToServer();
}

void TestGame::setGameScene()
{
    /*
     * Ovde se predlaze postavljanje inicijalne scene.
    */

    scene()->addItem(new TestExitRect());
    scene()->addRect(0,0, 1, 1);

    qDebug() << "Hello from TestGame";
}

void TestGame::connectedToServer()
{
    /*
     * Pokrece se kada se povezemo sa serverom.
     *
     * Ukoliko zelimo da pokrenemo igru u multiplayeru,
     * ovde treba pozvati initNewGame, tako trazimo od
     * servera da nas smesti u red cekanja za nasu igru.
    */

    qDebug() << "Connected to server";
}

void TestGame::disconnectedFromServer()
{
    /*
     * Metod ce biti pozvan ukoliko dodje do
     * "regularnog" prekida konekcije
     * (mi prekidamo vezu, server se ugasio i sl).
    */
    qDebug() << "Disconnected from server";
}

void TestGame::gameInit(int player, int gameIndex)
{
    /*
     * Vec cekamo u redu za igru, server je uspeo
     * da nas upari sa nekim igracem.
     * Odredio je ko je koji igrac.
     *
     * Ulazni argumenti sluze samo da se proslede
     * verziji funkcije od nadklase, ona sve podesava.
     *
     * Jedino sto treba uciniti je implementirati je
     * reagovanje na pocetak igre.
    */

    Game::gameInit(player, gameIndex);

    /*
     * Ovde mozemo npr. obavestiti da nas je server
     * upario sa drugim igracem i igra je pocela.
    */
}

void TestGame::opponentMove(const QJsonObject &move)
{
    /*
     * Drugi igrac nam je prosledio svoj potez,
     * ovde reagujemo na njega.
    */

    // Obrada poteza ...

    // Obrada naseg poteza ...

    // Kreiranje i slanje poruke

    /*
     * Napomena:
     * Poruka ne sme da sadrzi polja 'type', 'outcome', 'gameIndex'.
     * To su kontrolni podaci i podesice se interno pozivom sendMove.
    */

    QJsonObject msg;
    msg["move"] = 2356;

    // Drugi argument je enum tip (pogledati definiciju Game).
    // Ako igra jos nije zavrsena konacnim rezultatom, poslati Invalid.
    sendMove(msg, Invalid);

    Q_UNUSED(move)
}

void TestGame::onNewGameOffer()
{
    /*
     * Igra se je zavrsila na regularan nacin.
     * Server nas je stavio u opstu listu klijenata
     * i pita nas da li zelimo da igramo opet.
     *
     * U potvrdnom, ovde treba pozvati initNewGame
     * i server ce nas smestiti u red za cekanje.
     *
     * U suprotnom, prekinuti vezu sa serverom.
    */
}

void TestGame::onGameInterrupted()
{
    /*
     * Igra je bila u toku i drugi igrac je
     * prekinuo vezu. Server nas je vratio u red za
     * cekanje i obavestava nas o tome.
     *
     * Ovde se predlaze neki ispis o tom dogodku,
     * resetovanje scene/table i slicno.
    */
}

void TestGame::onConnectionError(QAbstractSocket::SocketError socketError)
{
    /*
     * Ovde reagujemo na greske sa konekcijom
     * koje su u grupi "neregularnih".
     *
     * Obavezno prvo pozvati metod error iz nadklase
     * i proslediti mu argument. On ce generisati poruku
     * o gresci koju mozemo dobiti getter metodom ispod.
     *
     * Napomena: cak i kada npr. mi sami prekinemo vezu,
     * to se smatra greskom. Medjutim, funkcija nadklase
     * ce u takvim "regularnim" slucajevima postaviti errorMessage
     * na nullptr - stoga provera toga ispod.
     *
     * Ovakvi slucajevi, kao i kada mi sami prekinemo vezu,
     * obradjuju se u slotu disconnectedFromServer.
     *
     * Zato bi ova funkcija trebalo da sadrzi naredne redove i
     * nakon toga obraditi te ostale greske.
    */

    Game::error(socketError);
    QString errorMsg = errorMessage();
    if(errorMsg == nullptr) {
        return;
    }

    qDebug() << errorMsg;
}
