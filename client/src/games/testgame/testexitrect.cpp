#include <headers/games/testgame/testexitrect.h>

#include <headers/games/game.h>

TestExitRect::TestExitRect()
{

}

QRectF TestExitRect::boundingRect() const
{
    return m_rect;
}

void TestExitRect::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->drawRect(m_rect);

    Q_UNUSED(option);
    Q_UNUSED(widget);
}

int TestExitRect::type() const
{
    return Game::ExitGame;
}
