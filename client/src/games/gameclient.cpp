#include "../../headers/games/gameclient.h"

#include <QDataStream>
#include <QJsonDocument>

GameClient::GameClient(QObject *parent)
    : QObject(parent)
    , m_clientSocket(new QTcpSocket(this))
{
    connect(m_clientSocket, &QTcpSocket::connected, this, &GameClient::connected);
    connect(m_clientSocket, &QTcpSocket::disconnected, this, &GameClient::disconnected);
    connect(m_clientSocket, &QTcpSocket::readyRead, this, &GameClient::onReadyRead);
    connect(m_clientSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::errorOccurred),
            this, &GameClient::error);
}

/**
 * @brief GameClient::disconnect
 *
 * Prekida vezu sa serverom.
 */
void GameClient::disconnect()
{
    m_clientSocket->disconnectFromHost();
}

/**
 * @brief GameClient::isConnected
 * @return
 *
 * Vraca odgovor na pitanje da li je klijent povezan sa serverom ili ne.
 */
bool GameClient::isConnected()
{
    return m_clientSocket->state() == QAbstractSocket::ConnectedState;
}

/**
 * @brief GameClient::connectToServer
 * @param address
 * @param port
 *
 * Povezuje se sa serverom.
 */
void GameClient::connectToServer()
{
    const QString serverAddress = "127.0.0.1";
    const int port = 1967;
    m_clientSocket->connectToHost(serverAddress, port);
}

/**
 * @brief GameClient::sendJson
 * @param jsonObj
 *
 * Serveru salje poruku u JSON formatu.
 * Ne pozivati direktno u implementaciji igre
 * vec koristiti virtuelne metode iz klase Game.
 */
void GameClient::sendJson(const QJsonObject& jsonObj)
{
    Q_ASSERT(m_clientSocket);
    const QByteArray jsonData = QJsonDocument(jsonObj).toJson(QJsonDocument::Compact);

    QDataStream socketStream(m_clientSocket);
    socketStream.setVersion(QDataStream::Qt_5_12);
    socketStream << jsonData;
}

/**
 * @brief GameClient::jsonReceived
 * @param jsonObj
 *
 * GameClient je interno poziva ako uspe da procita poruku u JSON formatu sa soketa.
 */
void GameClient::jsonReceived(const QJsonObject &jsonObj)
{
    // Polje 'type' je obavezno
    const QJsonValue typeVal = jsonObj.value("type");
    if(typeVal.isNull() || !typeVal.isString()) {
        return;
    }

    if(typeVal.toString().compare("gameInit", Qt::CaseInsensitive) == 0) {

        // Po spajanju dva klijenta igru, server im salje poruku o tome ko je koji igrac.
        // polje 'player' ima vrednost 1 ili 2

        const QJsonValue playerVal = jsonObj.value("player");

        if(playerVal.isNull() || !playerVal.isDouble()) {
            return;
        }
        const int player = playerVal.toInt();

        // gameIndex je kljuc po kome se cuva instanca igre na serveru
        const QJsonValue gameIndexVal = jsonObj.value("gameIndex");

        if(gameIndexVal.isNull() || !gameIndexVal.isDouble()) {
            return;
        }
        const int gameIndex = gameIndexVal.toInt();

        emit gameInit(player, gameIndex);

    } else if(typeVal.toString().compare("play", Qt::CaseInsensitive) == 0) {

        // Drugi igrac je odigrao potez i poslao podatke o tome
        emit opponentMove(jsonObj);

    } else if(typeVal.toString().compare("playAgain", Qt::CaseInsensitive) == 0) {

        // Po pravilnom zavrsetku igre, server pita svakog od igraca da li zele igrati opet
        emit newGameOffer();

    } else if(typeVal.toString().compare("gameInterrupted", Qt::CaseInsensitive) == 0) {

        // Igra se je prekinula (verovatno je drugi igrac prekinuo vezu),
        // server drugom igracu salje poruku o tome i smesta ga ponovo u red za cekanje
        emit gameInterrupted();
    }
}

/**
 * @brief GameClient::onReadyRead
 *
 * Slot za pristigle podatke sa soketa.
 * Pokusava da procita poruku u JSON formatu i poziva jsonReceived.
 */
void GameClient::onReadyRead()
{
    QByteArray jsonData;
    QDataStream socketStream(m_clientSocket);
    socketStream.setVersion(QDataStream::Qt_5_12);

    while(true) {
        socketStream.startTransaction();
        socketStream >> jsonData;
        if(socketStream.commitTransaction()) {

            QJsonParseError parseError;
            const QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData, &parseError);
            if(parseError.error == QJsonParseError::NoError && jsonDoc.isObject()) {

                 jsonReceived(jsonDoc.object());

            }

        } else {
            break;
        }
    }
}
