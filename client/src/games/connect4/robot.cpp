#include "../../../headers/games/connect4/robot.h"
#include <climits>

Robot::Robot(const Game::Player& robotToken, const unsigned depth, const QVector<QVector<Token*>>& tokens, QObject* parent)
    : QThread(parent)
    , m_robotToken(robotToken)
    , m_depth(depth)
    , m_tokens(tokens)
{

}

void Robot::run()
{
    getRobotMove();
}

void Robot::getRobotMove()
{
    QVector<QVector<int>> state;
    QVector<int> top;
    boardToState(m_tokens, state, top);

    MinMaxNode move;
    int alpha = std::numeric_limits<int>().min();
    int beta = std::numeric_limits<int>().max();

    if(m_robotToken == Game::Player1) {
        // Za crveni zeton se maksimizira
        move = maximize(state, top, 1, m_depth, alpha, beta);

    } else {
        // Za zuti se minimizira
        move = minimize(state, top, 2, m_depth, alpha, beta);
    }

    /*
     * Ukoliko racunar predvidi da je izgubio, po minimaxu
     * mu je "svejedno" sta ce odigrati jer racuna da ga protivnik
     * pobedjuje u sledecem potezu.
     *
     * Verovatno ce izabrati prvu slobodnu kolonu, sto ne samo
     * da izgleda glupo, nego je vrlo moguce da korisnik nije ni
     * video da ima priliku za pobedu.
     *
     * Racunar pokusava da se bori svejedno, sto bi odgovaralo
     * ponasanju ljudskog igraca.
     * Poziva se minimax algoritam dubine 1, cisto da se vidi koji
     * potez bi trebalo da odigra protivnik za pobedu. Racunar bar
     * odlaze taj ishod zauzimanjem tog polja.
     */

    if((m_robotToken == Game::Player1 && move.value == -1000)
            || (m_robotToken == Game::Player2 && move.value == 1000)) {

        MinMaxNode tmp = m_robotToken == Game::Player1
                            ?   minimize(state, top , 2, 1, alpha, beta)
                            :   maximize(state, top , 1, 1, alpha, beta);

        state[top[tmp.col]--][tmp.col] = m_robotToken == Game::Player1 ? 2 : 1;

        int score = evaluate(state, top);
        state[++top[tmp.col]][tmp.col] = 0;

        if(score == move.value) {
            move.col = tmp.col;
        }
    }

    /*
     * Problem nastaje kada racunar nadje pobedu npr. vec u najlevljoj grani
     * ali na vecoj dubini i vrsi odsecanje, iako bi mogao vec u jednom potezu
     * izboriti pobedu u nekoj drugoj koloni.
     *
     * U narednom bloku se resava takav slucaj. "Rucno" se odigravaju potezi
     * za moguce kolone i ako se u jednoj ostvaruje pobeda, bira se ta kolona.
     */

    if((m_robotToken == Game::Player1 && move.value == 1000)
            || (m_robotToken == Game::Player2 && move.value == -1000)) {

        for(auto j=0; j<state[0].size(); j++) {
            if(top[j] != -1) {

                state[top[j]--][j] = m_robotToken == Game::Player1 ? 1 : 2;

                int score = evaluate(state, top);
                state[++top[j]][j] = 0;

                if(score == move.value) {
                    move.col = j;
                    break;
                }
            }
        }
    }

    emit robotMadeMove(move.col);
}

/**
 * @brief Robot::boardToState
 * @param tokens Matrica zetona
 * @param state Matrica int vrednost
 * @param top Niz prvih slobodnih mesta po kolonama (vrhovi)
 *
 * Generise "brzu" matricu stanja za minimax algoritam, kao i niz top
 */
void Robot::boardToState(const QVector<QVector<Token *> > &tokens, QVector<QVector<int> > &state, QVector<int>& top)
{
    // Punjenje matrice stanja
    for(auto& boardRow : tokens) {

        state.push_back(QVector<int>());
        for(auto& cell : boardRow) {
            int cellVal;
            switch(cell->player()) {
            case Game::Player1: {
                cellVal = 1;
                break;
            }
            case Game::Player2: {
                cellVal = 2;
                break;
            }
            case Game::Invalid: {
                cellVal = 0;
                break;
            }
            default:
                break;
            }

            state.back().push_back(cellVal);
        }
    }

    // Niz top - za svaku kolonu se cuva indeks reda prvog slobodnog mesta odozgo
    // Ako je red pun, vrednost je -1

    // Na pocetku su vrednosti svuda 5 (prazna tabla), redovi su indeksirani odozgi 0-5
    top = QVector<int>(state[0].size(), state.size()-1);
    for(auto j=0; j<top.size(); j++) {
        // Proveravaju se celije od leve kolone na desno,
        // od donje celije na gore
        for(auto i=state.size()-1; i>=0; i--) {
            if(state[i][j] == 0) {
                break;
            }

            top[j]--;
        }
    }
}

/**
 * @brief Robot::evaluate
 * @param state Referenca na matricu stanja
 * @param top Referenca na niz prvih slobodnih mesta u koloni
 * @return Ceo broj u intervalu [-1000,1000]. Te granice su za slucajeve pobede/poraza.
 * Metod racuna i medju-stanja i boduje igraca/robota shodno njima.
 */
int Robot::evaluate(const QVector<QVector<int> > &state, const QVector<int>& top)
{
    /*
        Proverava se da li ima 4 u nizu po redovima.
        Ako u i-tom redu u koloni j=3 nema zetona, nema sigurno pobednika u tom redu,
        pa ni u gornjim redovima.
    */
    int i,j;
    for(i=state.size()-1; i>=0 && state[i][3] != 0; i--) {
        for(j=3; j<state[0].size(); j++) {
            if(state[i][j] == state[i][j-1] && state[i][j-1] == state[i][j-2] &&
                    state[i][j-2] == state[i][j-3]) {

                return state[i][j] == 1 ? 1000 : -1000;
            }
        }
    }

    /*
        Proverava se po kolonama.
        Ako na polju (i,j) nema zetona, tu nema 4 u nizu, sece se i pretraga za vise redove
    */
    for(j=0; j<state[0].size(); j++) {
        for(i=2; i>=0 && state[i][j] != 0; i--) {
            if(state[i][j] == state[i+1][j] && state[i+1][j] == state[i+2][j] &&
                    state[i+2][j] == state[i+3][j]) {

                return state[i][j] == 1 ? 1000 : -1000;
            }
        }
    }

    // Slicno za dijagonale oblika '/'
    for(j=3; j<state[0].size(); j++) {
        for(i=2; i>=0 && state[i][j] != 0; i--) {
            if(state[i][j] == state[i+1][j-1] && state[i+1][j-1] == state[i+2][j-2] &&
                    state[i+2][j-2] == state[i+3][j-3]) {

                return state[i][j] == 1 ? 1000 : -1000;
            }
        }
    }

    // Slicno za dijagonale oblika '\'
    for(j=3; j>=0; j--) {
        for(i=2; i>=0 && state[i][j] != 0; i--) {
            if(state[i][j] == state[i+1][j+1] && state[i+1][j+1] == state[i+2][j+2] &&
                    state[i+2][j+2] == state[i+3][j+3]) {

                return state[i][j] == 1 ? 1000 : -1000;
            }
        }
    }

    /*
        Proverava se da li postoje 3 u nizu i bar jedna pozicija sa neke strane gde se moze
        odigrati za pobedu.

        Npr za horizontalu, 3 u nizu za j=[1,2,3] u redu i:
        Ako je samo jedan od top[0] i top[4] jednak 0, to nosi 5 odnosno -5 poena.
        Ako dodatno vazi top[0]=top[4]=i, to je "mat" i nosi 50/-50 poena.
        Medjutim potrebno je prvo proveriti sve horizontale, i dijagonale za slucajeve 50/-50
        pa tek onda za slucajeve 5/-5. Dodatno postoje posebni slucajevi na dijagonalama i
        svim kolonama gde nije moguce osvojiti 50/-50.
        Posto se ovakvi slucajevi mogu ponavljati za oba igraca u jednom istom stanju,
        poeni se sabiraju.
    */

    int score = 0;

    // Prvo se gleda horizontalno za 50/-50
    for(i=state.size()-1; i>=0 && state[i][3] != 0; i--) {
        for(j=3; j<state[0].size()-1; j++) {
            if(state[i][j] == state[i][j-1] && state[i][j-1] == state[i][j-2] &&
                    top[j-3] == i && top[j+1] == i) {

                score += state[i][j] == 1 ? 50 : -50;
            }
        }
    }

    // Slicno za dijagonale oblika '/'
    for(j=3; j<state[0].size()-1; j++) {
        for(i=2; i>0; i--) {
            if(state[i][j]!=0 && state[i][j] == state[i+1][j-1] &&
                    state[i+1][j-1] == state[i+2][j-2] && top[j-3] == (i+3) &&
                    top[j+1] == (i-1)) {

                score += state[i][j] == 1 ? 50 : -50;
            }
        }
    }

    // Slicno za dijagonale oblika '\'
    for(j=3; j<state[0].size()-1; j++) {
        for(i=4; i>2; i--) {
            if(state[i][j]!=0 && state[i][j] == state[i-1][j-1] &&
                    state[i-1][j-1] == state[i-2][j-2] && top[j+1] == (i+1) &&
                    top[j-3] == (i-3)) {

                score += state[i][j] == 1 ? 50 : -50;
            }
        }
    }

    // Traze se slucajevi za 5/-5 poena.

    // Opsti slucajevi za horizontale
    for(i=state.size()-1; i>=0 && state[i][3] != 0; i--) {
        for(j=3; j<state[0].size()-1; j++) {
            if(state[i][j] == state[i][j-1] && state[i][j-1] == state[i][j-2] &&
                    (top[j-3] == i || top[j+1] == i)) {

                score += state[i][j] == 1 ? 5 : -5;
            }
        }
    }

    //  Specijalni slucajevi za horizontale
    for(i=state.size()-1; i>=0; i--) {
        if(state[i][0]!=0 && state[i][0] == state[i][1] &&
                state[i][1] == state[i][2] && top[3] == i) {

            score += state[i][0] == 1 ? 5 : -5;
        }

        if(state[i][6]!=0 && state[i][6] == state[i][5] &&
                state[i][5] == state[i][4] && top[3] == i) {

            score += state[i][6] == 1 ? 5 : -5;
        }
    }

    // Kolone ne mogu dati 50/-50 ni u kom slucaju
    for(j=0; j<state[0].size(); j++) {
        for(i=3; i>0 && state[i][j] != 0; i--) {
            if(state[i][j] == state[i+1][j] && state[i+1][j] == state[i+2][j] &&
                    top[j] == (i-1)) {

                score += state[i][j] == 1 ? 5 : -5;
            }
        }
    }

    // Dijagonale oblika '/'
    for(j=3; j<state[0].size()-1; j++) {
        for(i=2; i>0; i--) {
            if(state[i][j]!=0 && state[i][j] == state[i+1][j-1] &&
                    state[i+1][j-1] == state[i+2][j-2] &&
                    (top[j-3] == (i+3) || top[j+1] == (i-1))) {

                score += state[i][j] == 1 ? 5 : -5;
            }
        }
    }

    // Dijagonale oblika '\'
    for(j=3; j<state[0].size()-1; j++) {
        for(i=4; i>2; i--) {
            if(state[i][j]!=0 && state[i][j] == state[i-1][j-1] &&
                    state[i-1][j-1] == state[i-2][j-2] &&
                    (top[j+1] == (i+1) || top[j-3] == (i-3))) {

                score += state[i][j] == 1 ? 5 : -5;
            }
        }
    }

    // Specijalni slucajevi za sve dijagonale uz levi i desni rub
    for(i=3; i>0; i--) {
        // Oblik '/' levi rub
        if(state[i][2]!=0 && state[i][2] == state[i+1][1] &&
                state[i+1][1] == state[i+2][0] && top[3] == (i-1)) {

            score += state[i][2] == 1 ? 5 : -5;
        }

        // Oblik '\' levi rub
        if(state[i+1][2]!=0 && state[i+1][2] == state[i][1] &&
                state[i][1] == state[i-1][0] && top[3] == (i+2)) {

            score += state[i+1][2] == 1 ? 5 : -5;
        }

        // Oblik '/' desni rub
        if(state[i+1][4]!=0 && state[i+1][4] == state[i][5] &&
                state[i][5] == state[i-1][6] && top[3] == (i+2)) {

            score += state[i+1][4] == 1 ? 5 : -5;
        }

        // Oblik '\' desni rub
        if(state[i][4]!=0 && state[i][4] == state[i+1][5] &&
                state[i+1][5] == state[i+2][6] && top[3] == (i-1)) {

            score += state[i][4] == 1 ? 5 : -5;
        }
    }

    // Specijalni slucajevi za sve dijagonale uz gornji i donji rub
    for(j=2; j<state[0].size()-1; j++) {
        // Oblik '/' donji rub
        if(state[3][j]!=0 && state[3][j] == state[4][j-1] &&
                state[4][j-1] == state[5][j-2] && top[j+1] == 2) {

            score += state[3][j] == 1 ? 5 : -5;
        }

        // Oblik '\' donji rub
        if(state[5][j+1]!=0 && state[5][j+1] == state[4][j] &&
                state[4][j] == state[3][j-1] && top[j-2] == 2) {

            score += state[5][j+1] == 1 ? 5 : -5;
        }

        // Oblik '/' gornji rub
        if(state[0][j+1]!=0 && state[0][j+1] == state[1][j] &&
                state[1][j] == state[2][j-1] && top[j-2] == 3) {

            score += state[0][j+1] == 1 ? 5 : -5;
        }

        // Oblik '\' gornji rub
        if(state[2][j]!=0 && state[2][j] == state[1][j-1] &&
                state[1][j-1] == state[0][j-2] && top[j+1] == 3) {

            score += state[2][j] == 1 ? 5 : -5;
        }
    }

    return score;
}

Robot::MinMaxNode Robot::minimize(QVector<QVector<int> > &state, QVector<int> &top, int player, unsigned depth, int alpha, int beta)
{
    MinMaxNode node;
    node.value = evaluate(state, top);
    node.col = 0;

    // Proverava se da li je puna tabla tj. da li su svi top[j]=-1
    const bool boardFull = -7 == std::accumulate(top.begin(), top.end(), 0);

    // Izlaz iz rekurzije za 0-tu dubinu, ako postoji pobednik ili ako je puna tabla
    if(depth == 0 || node.value == 1000 || node.value == -1000 || boardFull)
        return node;

    node.value = std::numeric_limits<int>().max();


    for(auto j=0; j<top.size(); j++) {

        if(top[j] == -1) {
            // Kolona je puna, prelazi se na sledecu
            continue;
        }

        // Odigrava se potez
        state[top[j]--][j] = player;

        MinMaxNode maxVal = maximize(state, top, (player == 1 ? 2 : 1), depth-1, alpha, beta);

        // "Brise" se odigran potez jer je state referenca na stanje
        // i potrebno nam je originalno za naredne rekurzivne pozive
        state[++top[j]][j] = 0;

        if(maxVal.value < node.value) {
            node.value = maxVal.value;
            node.col = j;
            beta = std::min(node.value, beta);
        }

        // Alfa-beta odsecanje
        if(beta <= alpha)
            break;
    }

    return node;
}

Robot::MinMaxNode Robot::maximize(QVector<QVector<int> > &state, QVector<int> &top, int player, unsigned depth, int alpha, int beta)
{
    MinMaxNode node;
    node.value = evaluate(state, top);
    node.col = 0;

    // Proverava se da li je puna tabla tj. da li su svi top[j]=-1
    const bool boardFull = -7 == std::accumulate(top.begin(), top.end(), 0);

    // Izlaz iz rekurzije za 0-tu dubinu, ako postoji pobednik ili ako je puna tabla
    if(depth == 0 || node.value == 1000 || node.value == -1000 || boardFull)
        return node;

    node.value = std::numeric_limits<int>().min();


    for(auto j=0; j<top.size(); j++) {

        if(top[j] == -1) {
            // Kolona je puna, prelazi se na sledecu
            continue;
        }

        // Odigrava se potez
        state[top[j]--][j] = player;

        MinMaxNode minVal = minimize(state, top, (player == 1 ? 2 : 1), depth-1, alpha, beta);

        // "Brise" se odigran potez jer je state referenca na stanje
        // i potrebno nam je originalno za naredne rekurzivne pozive
        state[++top[j]][j] = 0;

        if(minVal.value > node.value) {
            node.value = minVal.value;
            node.col = j;
            alpha = std::max(node.value, alpha);
        }

        // Alfa-beta odsecanje
        if(beta <= alpha)
            break;
    }

    return node;
}

unsigned Robot::depth() const
{
    return m_depth;
}

