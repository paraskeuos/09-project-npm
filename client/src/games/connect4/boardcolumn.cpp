#include "../../../headers/games/connect4/boardcolumn.h"

#include <headers/games/connect4/circleframe.h>

BoardColumn::BoardColumn(int index, qreal width, qreal height)
    : m_rect(QRectF(-width/2, -height/2, width, height))
    , m_index(index)
{
    const qreal circleGap = (height - rowNum() * width) / (rowNum() - 1);

    CircleFrame* frame0 = new CircleFrame(m_rect.width()/2);
    frame0->setParentItem(this);
    frame0->setPos(0, -2*width - width/2 - 2*circleGap - circleGap/2);

    CircleFrame* frame1 = new CircleFrame(m_rect.width()/2);
    frame1->setParentItem(this);
    frame1->setPos(0, -width - width/2 - circleGap - circleGap/2);

    CircleFrame* frame2 = new CircleFrame(m_rect.width()/2);
    frame2->setParentItem(this);
    frame2->setPos(0, -width/2 - circleGap/2);

    CircleFrame* frame3 = new CircleFrame(m_rect.width()/2);
    frame3->setParentItem(this);
    frame3->setPos(0, width/2 + circleGap/2);

    CircleFrame* frame4 = new CircleFrame(m_rect.width()/2);
    frame4->setParentItem(this);
    frame4->setPos(0, width + width/2 + circleGap + circleGap/2);

    CircleFrame* frame5 = new CircleFrame(m_rect.width()/2);
    frame5->setParentItem(this);
    frame5->setPos(0, 2*width + width/2 + 2*circleGap + circleGap/2);

    setAcceptHoverEvents(true);
}

QRectF BoardColumn::boundingRect() const
{
    return m_rect;
}

void BoardColumn::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(painter);
    Q_UNUSED(option)
    Q_UNUSED(widget)
}

void BoardColumn::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    QGraphicsItem::hoverEnterEvent(event);

    emit columnHover(index());
    event->accept();
}

void BoardColumn::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    // Samo se prihvata dogadjaj da bi se aktivirao mouseReleaseEvent
    QGraphicsItem::mousePressEvent(event);
    event->accept();
}

void BoardColumn::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);

    emit columnClick(index());
}

QRectF BoardColumn::rect() const
{
    return m_rect;
}

int BoardColumn::rowNum() const
{
    return m_rowNum;
}

int BoardColumn::index() const
{
    return m_index;
}
