#include "../../../headers/games/connect4/connect4.h"

#include <QJsonObject>
#include <QPen>
#include <QPushButton>

#include <headers/games/paras_menuitem_lib/exitbutton.h>
#include <headers/games/connect4/gameboard.h>
#include <headers/games/paras_menuitem_lib/gameoverpanel.h>
#include <headers/games/paras_menuitem_lib/menubutton.h>
#include <headers/games/paras_menuitem_lib/messagefield.h>

Connect4::Connect4(QGraphicsView* parent, GameItem::GameId gameId)
    : Game(parent, gameId)
    , m_robot(nullptr)
{
    const GameClient* client = gameClient();
    connect(client, &GameClient::connected, this, &Connect4::connectedToServer);
    connect(client, &GameClient::disconnected, this, &Connect4::disconnectedFromServer);
    connect(client, &GameClient::error, this, &Connect4::onConnectionError);
    connect(client, &GameClient::gameInit, this, &Connect4::gameInit);
    connect(client, &GameClient::opponentMove, this, &Connect4::opponentMove);
    connect(client, &GameClient::newGameOffer, this, &Connect4::onNewGameOffer);
    connect(client, &GameClient::gameInterrupted, this, &Connect4::onGameInterrupted);

    scene()->setBackgroundBrush(QBrush("#b0aeab"));
    setMainMenu();
}

Connect4::~Connect4()
{
    // Posebno se brise robot jer nije deo scene
    if(m_robot) {
        m_robot->deleteLater();
    }
}

void Connect4::setGameScene()
{
    scene()->clear();
    currentPlayer(Player1);

    // Dimenzije dugmica
    const qreal width = 50;
    const qreal height = 20;

    // U gornjem desnom uglu je dugme za povratak na Dashboard
    MenuButton* backToMenuBtn = new MenuButton(scene()->width()/2 - width, -scene()->height()/2,
                                         width, height, MenuButton::BackToMenu, tr("Exit"));
    scene()->addItem(backToMenuBtn);
    connect(backToMenuBtn, &MenuButton::backToSPMenu, this, &Connect4::setMainMenu);

    // Dodaje se tabla za igru
    m_gameBoard = new GameBoard(scene()->width()/2);
    m_gameBoard->setPos(0, scene()->height()/7);
    connect(m_gameBoard, &GameBoard::checkValidHover, this, &Connect4::onColumnHover);
    connect(m_gameBoard, &GameBoard::pickedMove, this, &Connect4::onPickedMove);
    connect(m_gameBoard, &GameBoard::droppedToken, this, &Connect4::onTokenDropped);

    scene()->addItem(m_gameBoard);

    if(!multiplayer()) {
        player(Player1);
        if(m_robot) {
            m_robot->deleteLater();
        }
        // Inicijalizacija bota za single player
        m_robot = new Robot(Player2, m_depth, m_gameBoard->tokens(), this);
        connect(m_robot, &Robot::robotMadeMove, this, &Connect4::onRobotMove);
    }

    // Dodaje se objekat koji ce ispisivati kada je igrac na potezu
    m_turnInfo = new PlayerTurnInfo(-50, -scene()->sceneRect().height()/2 + 10, 100, 50);
    scene()->addItem(m_turnInfo);
    if(player() != currentPlayer()) {
        m_turnInfo->hide();
    }
}

/**
 * @brief Connect4::setMainMenu
 *
 * Postavlja glavni meni po ulasku u igru.
 * Ako je korisnik bio povezan na server, prekida se konekcija.
 */
void Connect4::setMainMenu()
{
    disconnectFromServer();
    scene()->clear();

    // Dimenzije dugmica
    const qreal width = 250;
    const qreal height = 60;
    // Vertikalni prostor izmedju dugmica
    const qreal btnGap = 20;

    MenuButton* singlePlayerBtn = new MenuButton(-width/2, -height/2 - btnGap - height, width, height,
                                                 MenuButton::SinglePlayer, tr("Single Player"));
    scene()->addItem(singlePlayerBtn);
    connect(singlePlayerBtn, &MenuButton::chooseMode, this, &Connect4::onChooseMode);

    MenuButton* multiPlayerBtn = new MenuButton(-width/2, -height/2, width, height,
                                                MenuButton::MultiPlayer, tr("Play Online"));

    scene()->addItem(multiPlayerBtn);
    connect(multiPlayerBtn, &MenuButton::chooseMode, this, &Connect4::onChooseMode);

    ExitButton* exitBtn = new ExitButton(-width/2, -height/2 + btnGap + height, width, height, tr("Quit"));
    scene()->addItem(exitBtn);
}

/**
 * @brief Connect4::setConnectScene
 *
 * @param message Obavestenje o statusu konekcije
 * @param pen Boja teksta, podrazumevano crna
 *
 * Poziva se ako korisnik izabere multiplayer mod.
 * Postavlja scenu sa tekstom u vezi sa konekcijom na server,
 * obavestenje o tome da je igrac smesten u red za cekanje,
 * eventualno neka greska.
 * Takodje je tu i dugme za povratak na glavni meni.
 */
void Connect4::setConnectScene(const QString& message, const bool isError)
{
    scene()->clear();
    player(Invalid);

    const qreal width = 100;
    const qreal height = 40;
    // y-rastojanje od centra za polje i dugme Back
    const qreal distCenter = 10;

    // Obavestenje
    MessageField* msgField = new MessageField(-scene()->sceneRect().width()/4, -2*height - distCenter,
                                              scene()->sceneRect().width()/2, height, message, isError);
    scene()->addItem((msgField));

    // Dugme za povratak na glavni meni
    MenuButton* backToMenuBtn = new MenuButton(-width/2, -height/2 + distCenter + height, width, height,
                                                 MenuButton::BackToMenu, tr("Back"));
    scene()->addItem(backToMenuBtn);
    connect(backToMenuBtn, &MenuButton::backToSPMenu, this, &Connect4::setMainMenu);
}

void Connect4::setGameOverPanel(const Game::Player &winner)
{
    // -1 - korisnik je izgubio
    // 0 - nereseno
    // 1 - pobeda
    int outcome;
    if(winner == player()) {
        outcome = 1;
    } else if (winner == Game::Draw) {
        outcome = 0;
    } else {
        outcome = -1;
    }

    const qreal panelWidth = 300;
    const qreal panelHeight = 200;

    QRectF panelRect = QRectF(-panelWidth/2, -panelHeight/2, panelWidth, panelHeight);
    GameOverPanel* panel = new GameOverPanel(panelRect, outcome, true);
    panel->setPos(0, -panelHeight);
    scene()->addItem(panel);

    const qreal btnWidth = 60;
    const qreal btnHeight = 40;
    const qreal btnGap = 50;

    MenuButton::ButtonType yesType = m_multiplayer ? MenuButton::PlayAgainMP : MenuButton::PlayAgainSP;
    MenuButton* yesBtn = new MenuButton(panelRect.center().x() - btnWidth - btnGap/2,
                                        panelRect.bottom() - btnHeight - btnGap/2,
                                        btnWidth, btnHeight, yesType, tr("Yes"));
    yesBtn->setPos(0, -panelHeight);
    scene()->addItem(yesBtn);

    MenuButton::ButtonType noType = m_multiplayer ? MenuButton::NoPlayAgainMP : MenuButton::NoPlayAgainSP;
    MenuButton* noBtn = new MenuButton(panelRect.center().x() + btnGap/2,
                                       panelRect.bottom() - btnHeight - btnGap/2,
                                       btnWidth, btnHeight, noType, tr("No"));
    noBtn->setPos(0, -panelHeight);
    scene()->addItem(noBtn);

    if(multiplayer()) {
        connect(yesBtn, &MenuButton::playAgainMP, this, &Connect4::playAgainMP);
        connect(noBtn, &MenuButton::noPlayAgainMP, this, &Connect4::noPlayAgainMP);

    } else {
        connect(yesBtn, &MenuButton::playAgainSP, this, &Connect4::playAgainSP);
        connect(noBtn, &MenuButton::noPlayAgainSP, this, &Connect4::noPlayAgainSP);
    }
}

/**
 * @brief Connect4::setSinglePlayerMenu
 *
 * Poziva se ako igrac izabere Single Player mod.
 * Daje se na izbor tezine igre.
 *
 */
void Connect4::setSinglePlayerMenu()
{
    scene()->clear();

    // Dimenzije dugmica
    const qreal width = 150;
    const qreal height = 50;
    // Vertikalni prostor izmedju dugmica
    const qreal btnGap = 20;
    // Distanca po vertikali za koju se svi dugmici pomeraju na gore
    const qreal distCenter = height/2 + btnGap/2;

    MenuButton* easyDiffBtn = new MenuButton(-width/2, -height/2 - btnGap - height - distCenter, width, height,
                                                 MenuButton::Easy, tr("Easy"));
    scene()->addItem(easyDiffBtn);

    MenuButton* mediumDiffBtn = new MenuButton(-width/2, -height/2 - distCenter, width, height,
                                                 MenuButton::Medium, tr("Medium"));
    scene()->addItem(mediumDiffBtn);

    MenuButton* hardDiffBtn = new MenuButton(-width/2, -height/2 + btnGap + height - distCenter, width, height,
                                                 MenuButton::Hard, tr("Hard"));
    scene()->addItem(hardDiffBtn);

    connect(easyDiffBtn, &MenuButton::startSPGame, this, &Connect4::onStartSPGame);
    connect(mediumDiffBtn, &MenuButton::startSPGame, this, &Connect4::onStartSPGame);
    connect(hardDiffBtn, &MenuButton::startSPGame, this, &Connect4::onStartSPGame);

    MenuButton* backToMenuBtn = new MenuButton(-width/2, -height/2 + 2*btnGap + 2*height - distCenter, width, height,
                                                 MenuButton::BackToMenu, tr("Back"));
    scene()->addItem(backToMenuBtn);
    connect(backToMenuBtn, &MenuButton::backToSPMenu, this, &Connect4::setMainMenu);
}

/**
 * @brief Connect4::onChooseMode
 * @param multiplayer
 *
 * Slot kojim se reaguje na izbor SinglePlayer/Multiplayer moda.
 */
void Connect4::onChooseMode(bool multiplayer)
{
    this->multiplayer(multiplayer);

    if(this->multiplayer()) {
        // Povezujemo se sa serverom
        setConnectScene(tr("Connecting to server..."), false);
        connectToServer();

    } else {
        setSinglePlayerMenu();
    }
}

void Connect4::onPickedMove(int index)
{
    // U ovu kolonu ima mesta za zeton, ali mi nismo na potezu
    if(player() != currentPlayer()) {
        return;
    }

    m_gameBoard->dropToken(index);
}

/**
 * @brief Connect4::onColumnHover
 * @param index Indeks kolone
 *
 * Proverava da li je dozvoljeno pomerati zeton,
 * tj da li je korisnikov ili tudji.
 */
void Connect4::onColumnHover(int index)
{
    if(player() == currentPlayer()) {
        m_gameBoard->moveHoverToken(index);
    }
}

/**
 * @brief Connect4::onTokenDropped
 * @param Indeks kolone odigranog poteza
 *
 * Proverava stanje igre i dalju logiku po odigranom potezu
 */
void Connect4::onTokenDropped(int index)
{
    // Gleda se da li je igra gotova
    const Player winner = m_gameBoard->checkState();

    if(multiplayer()) {
        // Proverava se da li je korisnik ili drugi igrac odigrao potez
        if(player() == currentPlayer()) {

            // Korisnik je odigrao potez
            // Drugom igracu se salje izabrana kolona
            QJsonObject move;
            move[tr("pickedCol")] = index;

            sendMove(move, winner);
        }

        // U slucaju konacnog ishoda blokiraju se dalji potezi
        if(winner != Player::Invalid) {

            // Onemogucava se dalje bacanje zetona
            currentPlayer(Invalid);
        } else {
            currentPlayer(currentPlayer() == Player1 ? Player2 : Player1);
        }

    } else {
        // U slucaju konacnog ishoda blokiraju se dalji potezi
        if(winner != Player::Invalid) {

            // Onemogucava se dalje bacanje zetona
            currentPlayer(Invalid);

            // Crta se GameOverPanel
            setGameOverPanel(winner);

            return;
        }

        if(player() == currentPlayer()) {
            // Upravo je odigrao korisnik

            // Sada se poziva racunar
            m_robot->start();
            // Blokira se odigravanje poteza dok ne odigra racunar
            currentPlayer(Invalid);

        } else if(currentPlayer() == Invalid && winner == Invalid) {
            // Racunar je upravo bacio zeton
            currentPlayer(currentPlayer() == Player1 ? Player2 : Player1);
        }
    }

    // Ako je sada korisnik na potezu, pokazuje se informacija o tome,
    // inace se skriva.
    if(player() == currentPlayer()) {
        m_turnInfo->show();
    } else {
        m_turnInfo->hide();
    }
}

/**
 * @brief Connect4::onStartSPGame
 * @param depth Dubina za minimax algoritam
 *
 * Slot koji reaguje na izbor tezine za single player.
 * Pokrece igru protiv racunara.
 */
void Connect4::onStartSPGame(unsigned depth)
{
    m_depth = depth;
    setGameScene();
}

/**
 * @brief Connect4::onRobotMove
 * @param col Indeks kolone
 *
 * Slot koji se pozove kada racunar izabere kolonu
 * za odigravanje svog poteza
 */
void Connect4::onRobotMove(unsigned col)
{
    m_gameBoard->dropToken(col);
}

void Connect4::playAgainSP()
{
    onStartSPGame(m_robot->depth());
}

void Connect4::noPlayAgainSP()
{
    setMainMenu();
}

void Connect4::playAgainMP()
{
    // Gotova je igra, server je pitao korisnika da li zeli
    // da igra opet, on je odgovorio sa 'da'.

    // Serveru se salje poruka da stavi korisnika u red za igru
    initNewGame();

    // Crta se obavestenje
    setConnectScene(tr("Waiting in queue..."), false);
}

void Connect4::noPlayAgainMP()
{
    // Gotova je igra, server je pitao korisnika da li zeli
    // da igra opet, on je odgovorio sa 'ne'.

    // Veza se automatski prekida kad odemo na glavni meni
    setMainMenu();
}

bool Connect4::multiplayer() const
{
    return m_multiplayer;
}

void Connect4::multiplayer(bool multiplayer)
{
    m_multiplayer = multiplayer;
}

void Connect4::connectedToServer()
{
    /*
     * Pokrece se kada se povezemo sa serverom.
     *
     * Ukoliko zelimo da pokrenemo igru u multiplayeru,
     * ovde treba pozvati initNewGame, tako trazimo od
     * servera da nas smesti u red cekanja za nasu igru.
    */

    // Uspesno povezivanje sa serverom, menja se poruka
    setConnectScene(tr("Connected, waiting in queue..."), false);

    //Saljemo zahtev da nas server smesti u red za cekanje
    initNewGame();
}

void Connect4::disconnectedFromServer()
{
    /*
     * Metod ce biti pozvan ukoliko dodje do
     * "regularnog" prekida konekcije
     * (mi prekidamo vezu, server se ugasio i sl).
    */
    setConnectScene(tr("Disconnected from server."), true);
}

void Connect4::gameInit(int player, int gameIndex)
{
    /*
     * Vec cekamo u redu za igru, server je uspeo
     * da nas upari sa nekim igracem.
     * Odredio je ko je koji igrac.
     *
     * Ulazni argumenti sluze samo da se proslede
     * verziji funkcije od nadklase, ona sve podesava.
     *
     * Jedino sto treba uciniti je implementirati je
     * reagovanje na pocetak igre.
    */

    Game::gameInit(player, gameIndex);

    /*
     * Ovde mozemo npr. obavestiti da nas je server
     * upario sa drugim igracem i igra je pocela.
    */

    setGameScene();
}

void Connect4::opponentMove(const QJsonObject &move)
{
    /*
     * Drugi igrac nam je prosledio svoj potez,
     * ovde reagujemo na njega.
    */

    // Obrada poteza ...
    const QJsonValue pickedColVal = move.value("pickedCol");
    if(pickedColVal.isNull() || !pickedColVal.isDouble()) {
        // Nije dospeo validan podatak o koloni
        disconnectFromServer();
        setConnectScene(tr("Error: Network packets corrupted."), true);

        return;
    }

    // Odigrava se potez drugog igraca
    m_gameBoard->dropToken(pickedColVal.toInt());
}

void Connect4::onNewGameOffer()
{
    /*
     * Igra se je zavrsila na regularan nacin.
     * Server nas je stavio u opstu listu klijenata
     * i pita nas da li zelimo da igramo opet.
    */

    // Iscrtava se panel sa informacijama
    setGameOverPanel(m_gameBoard->checkState());
}

void Connect4::onGameInterrupted()
{
    /*
     * Igra je bila u toku i drugi igrac je
     * prekinuo vezu. Server nas je vratio u red za
     * cekanje i obavestava nas o tome.
    */

    // Obavestava se korisnik
    setConnectScene(tr("Your opponent disconnected. Waiting in queue..."), false);
}

void Connect4::onConnectionError(QAbstractSocket::SocketError socketError)
{
    /*
     * Ovde reagujemo na greske sa konekcijom
     * koje su u grupi "neregularnih".
     *
     * Obavezno prvo pozvati metod error iz nadklase
     * i proslediti mu argument. On ce generisati poruku
     * o gresci koju mozemo dobiti getter metodom ispod.
     *
     * Napomena: cak i kada npr. mi sami prekinemo vezu,
     * to se smatra greskom. Medjutim, funkcija nadklase
     * ce u takvim "regularnim" slucajevima postaviti errorMessage
     * na nullptr - stoga provera toga ispod.
     *
     * Ovakvi slucajevi, kao i kada mi sami prekinemo vezu,
     * obradjuju se u slotu disconnectedFromServer.
     *
     * Zato bi ova funkcija trebalo da sadrzi naredne redove i
     * nakon toga obraditi te ostale greske.
    */

    Game::error(socketError);
    QString errorMsg = errorMessage();
    if(errorMsg == nullptr) {
        return;
    }

    // Neka nepopravljiva greska sa vezom
    setConnectScene(errorMsg, true);
}
