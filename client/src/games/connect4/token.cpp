#include "../../../headers/games/connect4/token.h"

#include <QBrush>

Token::Token(qreal width)
    : m_rect(QRectF(-width/2, -width/2, width, width))
{

}

QRectF Token::boundingRect() const
{
    return m_rect;
}

void Token::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if(player() == Game::Invalid) {
        return;
    }

    QPen pen;
    QBrush brush;
    if(player() == Game::Player1) {
        pen = QPen(Qt::darkRed);
        brush = QBrush(Qt::darkRed);
    } else {
        pen = QPen(Qt::darkYellow);
        brush = QBrush(Qt::darkYellow);
    }

    painter->setPen(pen);
    painter->setBrush(brush);
    painter->drawEllipse(m_rect);

    Q_UNUSED(option)
    Q_UNUSED(widget)
}

Game::Player Token::player() const
{
    return m_player;
}

void Token::player(const Game::Player &player)
{
    m_player = player;
}

void Token::dropStep(const qreal &dropStep)
{
    m_dropStep = dropStep;
}

unsigned Token::dropPhase() const
{
    return m_dropPhase;
}

void Token::dropPhase(unsigned dropPhase)
{
    m_dropPhase = dropPhase;
}

qreal Token::dropStep() const
{
    return m_dropStep;
}
