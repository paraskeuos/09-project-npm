#include "headers/games/sibicarenje/box.h"
#include "headers/games/sibicarenje/sibicarenje.h"

#include <QPainter>
#include <QThread>

#include <math.h>

Box::Box(CurrentBox currentBox): m_currentBox(currentBox)
{
    m_closedBoxPicture = QImage(":assets/images/games/sibicarenje/box1.png");
    m_openedBoxPicture = QImage(":assets/images/games/sibicarenje/box2.png");
    m_currentBoxPicture = &m_closedBoxPicture;

    m_canOpenBox = true;
    m_boxIsOpened = false;
    m_hasTheBall = false;
    m_isRotating = false;
    m_rotateSide = NONE;
    m_timer = nullptr;

    setPos(Box::boxPositions[currentBox]);
}

void Box::blockOpeningBox(bool flag)
{
    m_canOpenBox = !flag;
}

void Box::prepareNewRotation(CurrentBox box, qint16 side, qint16 level)
{
    if(m_currentBox != box)
        return;
    if(m_timer!=nullptr && m_timer->isActive())
        return;
    m_rotateSide = side;
    m_isRotating = true;
    // pravi se tajmer koji ce se okidati na sve manje vremena u zavisnosti od nivoa
    // on poziva funkciju rotateBox koja rotira kutiju za odredjeni ugao, a rotacija
    // ce biti brza kako je nivo veci
    m_timer = new QTimer();
    connect(m_timer, &QTimer::timeout, this, &Box::rotateBox);
    m_timer->start(30-2*level);
}

void Box::rotateBox()
{
    // pogledati Sibicarenje::chooseNextRotation za detaljnije objasnjenje o potrebi ovog if-a
    if(m_rotateSide==NONE){
        m_timer->stop();
        delete m_timer;
        m_timer = nullptr;
        m_isRotating = false;
        emit finishedRotating();

        QThread::msleep(500);
        return;
    }

    // pozicija na kojoj kutija treba da zavrsi
    QPointF endingPos;
    // tacka oko koje se kutija rotira, bice sredisnja tacka izmedju dve kutije
    // izmedju kojih se vrsi rotacija
    QPointF center;
    // promenljive koje su uvedene samo zarad lepseg koda
    QPointF firstBoxPos = Box::boxPositions[0];
    QPointF secondBoxPos = Box::boxPositions[1];
    QPointF thirdBoxPos = Box::boxPositions[2];
    // ugao rotacije, i od njega zavisi brzina pa je podesen isprobavanjem razlicitih vrednosti
    qreal angle = -9;

    // ako je rotacija na levu stranu, onda druga kutija ide na mesto prve,
    // treca na mesto druge, a prva na mesto trece
    // smatra se da su kutije ciklicno povezane, pa ce zbog toga rotacija ulevo
    // prve kutije biti prebacivanje skroz na desno, na mesto trece kutije
    if(m_rotateSide == LEFT_SIDE){
        if(m_currentBox == CurrentBox::SecondBox){
            endingPos = firstBoxPos;
            // y koordinata je ista za sve kutije pa se moze staviti proizvoljna kutija
            // dok je x koordinata aritmeticka sredina x koordinata dveju kutija, isto vazi
            // za sva ostala racunanja centra, uz koriscenje odgovarajucih x koordinata
            center = QPointF( (firstBoxPos.x() + secondBoxPos.x())/2, firstBoxPos.y() );
        } else if(m_currentBox == CurrentBox::ThirdBox){
            endingPos = secondBoxPos;
            center = QPointF( (secondBoxPos.x() + thirdBoxPos.x())/2, secondBoxPos.y() );
        } else{ // m_currentBox == CurrentBox::FirstBox
            endingPos = thirdBoxPos;
            center = QPointF( (thirdBoxPos.x() + firstBoxPos.x())/2, thirdBoxPos.y());
        }
    }
    // ako je rotacija na desnu stranu, onda prva kutija ide na mesto druge,
    // druga na mesto trece, a treca na mesto prve
    // smatra se da su kutije ciklicno povezane, pa ce zbog toga rotacija udesno
    // trece kutije biti prebacivanje skroz na levo, na mesto prve kutije
    else { // m_rotateSide==RIGHT_SIDE
        if(m_currentBox == CurrentBox::FirstBox){
            endingPos = secondBoxPos;
            center = QPointF( (secondBoxPos.x() + firstBoxPos.x())/2, secondBoxPos.y() );
        }
        else if(m_currentBox == CurrentBox::SecondBox){
            endingPos = thirdBoxPos;
            center = QPointF( (thirdBoxPos.x() + secondBoxPos.x())/2, thirdBoxPos.y() );
        } else{ // m_currentBox == CurrentBox::ThirdBox
            endingPos = firstBoxPos;
            center = QPointF( (firstBoxPos.x() + thirdBoxPos.x())/2, firstBoxPos.y()  );
        }
    }

    QTransform t;
    t.translate(center.x(), center.y());
    t.rotate(angle);
    t.translate(-center.x(), -center.y());

    // ako nismo dovolnjno blizu kranje pozicije
    // saljemo trenutnu poziciju i poziciju u koju ce se preslikati
    // kutija gornjom rotacijom
    if(posCloseTo(pos(), t.map(pos()), endingPos) == false){
        // azurira se nova pozicija i tajmer ce opet pozvati ovu funkciju
        this->setPos(t.map(this->pos()));
        this->update();
        return;
    }

    // ako je doslo do manje greske u racunu, potrebno je da se pozicija promeni
    // na onu pravu kako se greska kasnije ne bi akumulirala i dovela do vidljivog
    // izmestanja kutija
    this->setPos(endingPos);

    // zaustavljamo i unistavamo tajmer
    m_timer->stop();
    delete m_timer;
    m_timer = nullptr;

    // azuriramo informacije o tome na kom mestu pocetne kutije je trenutna kutija
    if(m_rotateSide == LEFT_SIDE){
        // ako je rotacija na levu stranu, onda druga kutija ide na mesto prve,
        // treca na mesto druge, a prva na mesto trece
        if(m_currentBox == CurrentBox::SecondBox)
            m_currentBox = CurrentBox::FirstBox;
        else if(m_currentBox == CurrentBox::ThirdBox)
            m_currentBox = CurrentBox::SecondBox;
        else // if(m_currentBox == CurrentBox::FirstBox)
            m_currentBox = CurrentBox::ThirdBox;
    } else{
        // ako je rotacija na desnu stranu, onda prva kutija ide na mesto druge,
        // druga na mesto trece, a treca na mesto prve
        if(m_currentBox == CurrentBox::FirstBox)
            m_currentBox = CurrentBox::SecondBox;
        else if(m_currentBox == CurrentBox::SecondBox)
            m_currentBox = CurrentBox::ThirdBox;
        else // if(m_currentBox == CurrentBox::ThirdBox)
            m_currentBox = CurrentBox::FirstBox;
    }

    m_rotateSide = NONE;
    m_isRotating = false;
    // emitujemo da smo zavrsili sa rotiranjem
    emit finishedRotating();
}


QRectF Box::boundingRect() const
{
    return m_boundingRect;
}

void Box::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
     painter->drawImage(boundingRect(), *m_currentBoxPicture,
                        boundingRect());

     Q_UNUSED(option);
     Q_UNUSED(widget);
}

CurrentBox Box::currentBox() const
{
    return m_currentBox;
}

bool Box::hasTheBall() const
{
    return m_hasTheBall;
}

void Box::hasTheBall(bool hasTheBall)
{
    m_hasTheBall = hasTheBall;
}

bool Box::isRotating() const
{
    return m_isRotating;
}

bool Box::isOpened() const
{
    return m_boxIsOpened;
}

void Box::openBox()
{
    if(isOpened() == false){
        m_currentBoxPicture = &m_openedBoxPicture;
        this->moveBy(0,-55);
        if(m_hasTheBall)
            emit showBall();
        // kada se jedna kutija otvori zelimo da blokiramo sve kutije
        emit sendBlockOpeningBoxSignal(true);
        m_boxIsOpened = true;
    }
}

void Box::closeBox()
{
    if(isOpened() == true){
        m_currentBoxPicture = &m_closedBoxPicture;
        this->moveBy(0,55);
        if(m_hasTheBall)
            emit hideBall();
        m_boxIsOpened = false;
    }
}

void Box::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    // sprecavamo da se pritiskanjem misa kada to nije dozvoljeno
    // otvaraju kutije
    if(m_canOpenBox == false)
        return;

    isOpened() == false ? openBox() : closeBox();

    // do ovde ce se doci iskljucivo kada je dozvoljeno otvaranje kutije,
    // a to ce biti kada je potrebno odabrati kutiju u kojoj je loptica
    // stoga, emituje se signal da smo trenutnu kutiju izabrali
    emit chosenBox(currentBox());
    QGraphicsItem::mousePressEvent(event);
    event->accept();
}

bool Box::posCloseTo(QPointF oldPos, QPointF newPos, QPointF endPos)
{
    // koristimo kvaziEuklidsko rastojanje, tj ne koristimo koren
    qreal firstPointDistance =
            (oldPos.x() - endPos.x()) * (oldPos.x() - endPos.x()) +
            (oldPos.y() - endPos.y()) * (oldPos.y() - endPos.y())
            ;
    qreal secondPointDistance =
            (newPos.x() - endPos.x()) * (newPos.x() - endPos.x()) +
            (newPos.y() - endPos.y()) * (newPos.y() - endPos.y())
            ;

    // ako je stara pozicija bliza krajnjoj nego nova pozicija
    // znaci da smo premasili krajnju poziciju i da treba da stanemo
    // funkcija vraca true ako treba zavrsiti rotaciju tj ako je oldPos
    // bliza endPos nego sto je to newPos
    if(firstPointDistance < secondPointDistance)
        return true;
    return false;
}
