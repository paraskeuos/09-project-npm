#include "headers/games/sibicarenje/ball.h"
#include "headers/games/sibicarenje/box.h"

#include <QPainter>

Ball::Ball(Box* box): m_box(box)
{
    // zelimo da loptica prati rotacije svoje kutije
    setParentItem(m_box);
    // postavljamo odgovarajucoj kutiji da sadrzi lopticu
    m_box->hasTheBall(true);
    // postavlja se inicijalna pozicija loptice
    setPos(Box::boxPositions[currentBox()] + m_vector);
}

QRectF Ball::boundingRect() const
{
    return m_boundingRect;
}

void Ball::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->drawImage(boundingRect(), QImage(":assets/images/games/sibicarenje/ball.png"),
                       boundingRect());
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

CurrentBox Ball::currentBox() const
{
    return m_box->currentBox();
}
