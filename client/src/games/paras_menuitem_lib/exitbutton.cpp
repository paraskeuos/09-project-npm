#include "../../../headers/games/paras_menuitem_lib/exitbutton.h"

#include <QPen>

#include <headers/games/game.h>


ExitButton::ExitButton(qreal x, qreal y, qreal width, qreal height, const QString& text, const QColor& color)
    : m_rect(QRectF(x, y, width, height))
    , m_text(text)
    , m_color(color)
{
}

QRectF ExitButton::boundingRect() const
{
    return m_rect;
}

void ExitButton::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->drawRect(m_rect);
    painter->fillRect(m_rect, QBrush(m_color));
    painter->drawText(m_rect, Qt::AlignCenter, m_text);

    Q_UNUSED(option)
    Q_UNUSED(widget)
}

int ExitButton::type() const
{
    return Game::ExitGame;
}
