#include "../../../headers/games/paras_menuitem_lib/menubutton.h"

#include <QDebug>

MenuButton::MenuButton(qreal x, qreal y, qreal width, qreal height, ButtonType type, const QString& text, const QColor& color)
    : m_rect(QRectF(x, y, width, height))
    , m_btnType(type)
    , m_text(text)
    , m_color(color)
{
}

QRectF MenuButton::boundingRect() const
{
    return m_rect;
}

void MenuButton::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->drawRect(m_rect);
    painter->fillRect(m_rect, QBrush(m_color));
    painter->drawText(m_rect, Qt::AlignCenter, m_text);

    Q_UNUSED(option)
    Q_UNUSED(widget)
}

MenuButton::ButtonType MenuButton::btnType() const
{
    return m_btnType;
}

void MenuButton::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);

    switch(btnType()) {
    case ButtonType::SinglePlayer: {
        emit chooseMode(false);
        break;
    }
    case ButtonType::MultiPlayer: {
        emit chooseMode(true);
        break;
    }
    case ButtonType::Easy: {
        emit startSPGame(3);
        break;
    }
    case ButtonType::Medium: {
        emit startSPGame(6);
        break;
    }
    case ButtonType::Hard: {
        emit startSPGame(8);
        break;
    }
    case ButtonType::BackToMenu: {
        emit backToSPMenu();
        break;
    }
    case ButtonType::ShowInstructions: {
        emit showInstructions();
        break;
    }
    case ButtonType::CloseInstructions: {
        emit closeInstructions();
        break;
    }
    case ButtonType::PlayAgainSP: {
        emit playAgainSP();
        break;
    }
    case ButtonType::NoPlayAgainSP: {
        emit noPlayAgainSP();
        break;
    }
    case ButtonType::PlayAgainMP: {
        emit playAgainMP();
        break;
    }
    case ButtonType::NoPlayAgainMP: {
        emit noPlayAgainMP();
        break;
    }
    default:
        break;
    }
}

void MenuButton::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    // Samo prihvatamo dogadjaj da bi se aktivirao mouseReleaseEvent
    QGraphicsItem::mousePressEvent(event);
    event->accept();
}
