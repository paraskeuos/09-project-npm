#include "../../../headers/games/paras_menuitem_lib/playerturninfo.h"

PlayerTurnInfo::PlayerTurnInfo(qreal x, qreal y, qreal width, qreal height)
    : m_rect(QRectF(x, y, width, height))
{

}

QRectF PlayerTurnInfo::boundingRect() const
{
    return m_rect;
}

void PlayerTurnInfo::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QFont font;
    font.setPixelSize(15);
    painter->setFont(font);

    painter->drawText(m_rect, Qt::AlignCenter, "Your turn");

    Q_UNUSED(option)
    Q_UNUSED(widget)
}
