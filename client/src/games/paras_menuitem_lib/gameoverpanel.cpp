#include "../../../headers/games/paras_menuitem_lib/gameoverpanel.h"

#include <QBrush>

#include <headers/games/paras_menuitem_lib/menubutton.h>

GameOverPanel::GameOverPanel(const QRectF rect, const int outcome, const bool multiplayer)
    : m_rect(rect)
    , m_outcome(outcome)
    , m_multiplayer(multiplayer)
{
    setEnabled(false);
}

QRectF GameOverPanel::boundingRect() const
{
    return m_rect;
}

void GameOverPanel::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QBrush brush = QBrush(Qt::white);
    painter->drawRect(m_rect);
    painter->fillRect(m_rect, brush);

    QString outcomeMsg;
    switch(m_outcome) {
    case -1:
        outcomeMsg = tr("You lost...");
        break;
    case 0:
        outcomeMsg = tr("Draw...");
        break;
    case 1:
        outcomeMsg = tr("You won!");
        break;
    default:
        break;
    }
    QFont font;
    font.setPixelSize(20);
    painter->setFont(font);
    painter->drawText(QRectF(m_rect.topLeft().x(), m_rect.topLeft().y(),
                                 m_rect.width(), m_rect.height()/3), Qt::AlignCenter, outcomeMsg);

    font.setPixelSize(15);
    painter->setFont(font);

    painter->setBackground(Qt::yellow);
    painter->drawText(QRectF(m_rect.topLeft().x(), m_rect.topLeft().y() + m_rect.height()/3,
                             m_rect.width(), m_rect.height()/3), Qt::AlignCenter, tr("Play again?"));

    Q_UNUSED(option)
    Q_UNUSED(widget)
}

MenuButton *GameOverPanel::yesBtn() const
{
    return m_yesBtn;
}

MenuButton *GameOverPanel::noBtn() const
{
    return m_noBtn;
}
