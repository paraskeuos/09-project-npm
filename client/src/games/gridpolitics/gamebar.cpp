#include "../../../headers/games/gridpolitics/gamebar.h"

GameBar::GameBar(const qreal x, const qreal y, const qreal width, const qreal height)
    : m_rect(QRectF(x, y, width, height))
    , m_currArmies(0)
{
}

QRectF GameBar::boundingRect() const
{
    return m_rect;
}

void GameBar::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->fillRect(m_rect, QBrush("#3c81aa"));

    const qreal centerX = m_rect.center().x();

    QFont font;
    font.setPixelSize(16);
    painter->setFont(font);

    painter->drawText(centerX - 150, m_rect.top(), 100, m_rect.height(), Qt::AlignCenter,
                      tr("Gold ") + QString::number(m_currGold) + tr(" (+") + QString::number(m_goldPerTurn) + tr(")"));

    painter->drawText(centerX + 20, m_rect.top(), 200, m_rect.height(), Qt::AlignCenter,
                      tr("Raised armies ") + QString::number(m_currArmies) + tr(" / ") + QString::number(m_maxArmies));

    Q_UNUSED(option)
    Q_UNUSED(widget)
}

int GameBar::currGold() const
{
    return m_currGold;
}

void GameBar::setCurrGold(int currGold)
{
    m_currGold = currGold;
}

int GameBar::goldPerTurn() const
{
    return m_goldPerTurn;
}

void GameBar::setGoldPerTurn(int goldPerTurn)
{
    m_goldPerTurn = goldPerTurn;
}

int GameBar::currArmies() const
{
    return m_currArmies;
}

void GameBar::setCurrArmies(int currArmies)
{
    m_currArmies = currArmies;
}

int GameBar::maxArmies() const
{
    return m_maxArmies;
}

void GameBar::setMaxArmies(int maxArmies)
{
    m_maxArmies = maxArmies;
}
