#include <headers/games/gridpolitics/gridbot.h>

#include <queue>

GridBot::GridBot(const Game::Player& robotPlayer, TileGrid* grid, QObject* parent)
    : QThread(parent)
    , m_botPlayer(robotPlayer)
    , grid(grid)
    , m_gold(0)
    , m_armies(0)
{

}

void GridBot::run()
{
   playMove();
}

Game::Player GridBot::botPlayer() const
{
    return m_botPlayer;
}

/**
 * @brief GridBot::playMove
 *
 * Jedini metod u interfejsu sa klasom GridBot
 * Poziva se da bi racunar odigrao svoj potez, tj akcije.
 * Sve dok ima zlatnika na raspolaganju, bot trazi najblizi
 * cvor koji nije u njegovom posedu a sadrzi ili zlatnik ili
 * neciji glavni grad.
 * Sve dok ima zlatnika na raspolaganju pokusava da osvoji sve
 * cvorove na putu do najblizeg cvora, vrsi mobilizaciju ako je
 * potrebno. Ukoliko uspe da osvoji ciljni cvor, nastavlja slicno
 * sa sledecim najblizim, sve dok ima zlatnika na raspolaganju.
 */
void GridBot::playMove()
{
    // Azurira se broj zlatnika i maksimum za vojsku
    setGold(getGold() + grid->getGoldPerTurn(botPlayer()));
    setMaxArmies(grid->getMaxArmies(botPlayer()));

    int closestTarget = -1;
    while(getGold() > 0) {
        // Bot vrsi akcije sve dok ima bar jedan zlatnik na raspolaganju
        if(closestTarget == -1 || grid->getTile(closestTarget)->owner() == botPlayer()) {
            // Ako je prvi ulazak u petlju ili je uspeo da osvoji ciljni cvor, trazi se novi
            closestTarget = findClosestTarget();
        }

        if(grid->getTile(closestTarget)->owner() != botPlayer()) {
            // Ako sledeci na putu ka ciljnom cvoru jos nije osvojen, pokusava se sa osvajanjem
            if(getArmies() >= grid->getTile(closestTarget)->defense()) {
                // Bot ima dovoljno vojske, osvaja cvor nextInPath
                occupyTile(closestTarget);

                if(grid->checkState() != Game::Invalid) {
                    // Bot je pobedio, kraj poteza
                    emit robotMadeMove();
                    return;
                }
            } else {
                // Ako nema dovoljno vojnika, vrsi se mobilizacija na jednom cvoru
                mobilize();
            }
        } else {
            // Cvor closestTarget je vec osvojen pa se trazi sledeca meta
            closestTarget = findClosestTarget();
        }
    }

    emit robotMadeMove();
}

/**
 * @brief GridBot::getBotBorderTiles
 * @param botTiles Indeksi granicnih cvorova u posedu bota
 *
 * Pregleda se grid i u vektor botTiles se ubacuju oni botovi cvorovi
 * koji ne pripadaju njegovoj unutrasnjoj teritoriji.
 */
void GridBot::getBotBorderTiles(const int gridSize, QVector<int>& botTiles)
{
    for(auto i=0; i<gridSize; i++) {
        if(grid->getTile(i)->owner() == botPlayer()) {
            // Ako je cvor u posedu bota, proverava se da li je granicni cvor
            // tj da li ima bar jednog suseda koji ne pripada botu
            for(auto neighbor : grid->getTile(i)->neighbors()) {
                if(grid->getTile(neighbor)->owner() != botPlayer()) {
                    botTiles.push_back(i);
                    break;
                }
            }
        }
    }
}

/**
 * @brief GridBot::getTargetTiles
 * @param targets Indeksi validnih ciljnih cvorova za osvajanje
 *
 * Validni ciljevi su oni cvorovi koji nisu u botovom posedu i ili
 * sadrze zlatnik ili neciji glavni grad - korisnikov za osvajanje
 * ili svoj za oslobodjenje.
 */
void GridBot::getTargetTiles(const int gridSize, QVector<int> &targets)
{
    for(auto i=0; i<gridSize; i++) {
        auto tile = grid->getTile(i);
        if(tile->owner() != botPlayer() && (tile->hasGold() || tile->whoseCapital() != Game::Invalid)) {
            // Validni ciljevi za bota su zlatnici ili neciji glavni grad.
            // Ako je protivnikov, blizi se pobedi, ako je njegov, to znaci
            // da ga je prethodno izgubio i sada zeli da ga povrati.
            targets.push_back(i);
        }
    }
}

/**
 * @brief GridBot::createAdjacencyList
 * @param borderTiles
 *
 * Promenljiva m_adjacencyList dobija najazurniju tezinsku listu susedstva za odlucivanje akcija.
 * Ne sadrzi unutrasnje cvorove botove teritorije.
 */
void GridBot::createAdjacencyList(const int gridSize, const QVector<int> &borderTiles)
{
    // Koristi se nova lista susedstva umesto stare ako je postojala
    m_adjacencyList.erase(std::begin(m_adjacencyList), std::end(m_adjacencyList));
    m_adjacencyList = QVector<QVector<std::pair<int,int>>>(gridSize, QVector<std::pair<int,int>>());

    for(auto i=0; i<gridSize; i++) {
        const auto tile = grid->getTile(i);
        if(tile->owner() == botPlayer() && !borderTiles.contains(i)) {
            // Unutrasnji cvor od bota, ne razmatra se
            continue;
        }

        for(auto neighbor : tile->neighbors()) {
            // "tezine" grana su defense vrednosti cvora preko kojeg se prelazi
            m_adjacencyList[i].push_back(std::pair<int,int>(neighbor, grid->getTile(neighbor)->defense()));
        }
    }
}

/**
 * @brief GridBot::findClosestTarget
 * @return Indeks najblizeg ciljneg cvora koji bot zeli da osvoji
 *
 * Enkapsulira niz postupaka za inicijalizaciju raznih kolekcija
 * potrebnih za izvrsavanje Dijkstrinog algoritma.
 * Izlaz je najblizi ciljni cvor pronadjen preko pomenutog algoritma.
 */
int GridBot::findClosestTarget()
{
    // Duzina niza grid se racuna samo jednom i prosledjuje gde je to potrebna
    const int gridSize = grid->sizeFactor()*grid->sizeFactor()
                        + (grid->sizeFactor()-1)*(grid->sizeFactor()-1);

    // Traze se granicni cvorovi botove teritorije
    QVector<int> botBorderTiles;
    getBotBorderTiles(gridSize, botBorderTiles);

    // Traze se ciljni cvorovi koje bot zeli da osvoji
    QVector<int> targetTiles;
    getTargetTiles(gridSize, targetTiles);

    // Kreira se tezinska lista susedstva za Dijsktrin algoritam
    createAdjacencyList(gridSize, botBorderTiles);

    // Dijkstrin algoritam pronalazi najblizi ciljni cvor
    //int closestTarget = closestTargetDijkstra(botBorderTiles, targetTiles);
    int closestTarget = closestTargetFloydWarshal(gridSize, botBorderTiles, targetTiles);

    return closestTarget;
}

/**
 * @brief GridBot::occupyTile
 * @param index Indeks cvora koji bot osvaja
 *
 * Bot osvaja zadati cvor i azurira stanje igre.
 */
void GridBot::occupyTile(const int index)
{
    grid->getTile(index)->owner(botPlayer());

    // Potrosio je onoliko bataljona koliko je i defense vrednost osvojenog cvora
    setArmies(getArmies() - grid->getTile(index)->defense());

    // Potrosen je i jedan zlatnik
    setGold(getGold() - 1);
}

/**
 * @brief GridBot::mobilize
 *
 * Bot vrsi mobilizaciju na jednom cvoru, i to na onom na kojem
 * ima najvise bataljona na raspolaganju.
 */
void GridBot::mobilize()
{
    // Sadrzace indekse botovih cvorova kod kojih je jos uvek moguca mobilizacija
    QVector<int> botTiles{};
    const int gridSize = grid->sizeFactor()*grid->sizeFactor()
                        + (grid->sizeFactor()-1)*(grid->sizeFactor()-1);

    for(auto i=0; i<gridSize; i++) {
        auto tile = grid->getTile(i);
        if(tile->owner() == botPlayer() && !tile->mobilizedTroops()) {
            // Validni su samo oni botovi cvorovi koji vec nisu mobilisani
            botTiles.push_back(i);
        }
    }

    // Niz botTiles se sortira nerastuce po broju bataljona.
    // Svaka mobilizacija kosta jedan zlatnik, zato je najbolji
    // mobilisati onakav cvor koji ima najvise bataljona na raspolaganju

    // const pokazivac zbog lambda funkcije
    const TileGrid* tmp = grid;
    std::sort(std::begin(botTiles), std::end(botTiles),
              [tmp](const int node1, const int node2) {
        return tmp->getTile(node1)->battalions() > tmp->getTile(node2)->battalions();
    });

    for(auto tile : botTiles) {
        if(getArmies() + grid->getTile(tile)->battalions() <= getMaxArmies()) {
            // Ako se ne premasuje vrednost m_maxArmies, vrsi mobilizacija
            setArmies(getArmies() + grid->getTile(tile)->battalions());
            grid->getTile(tile)->mobilizedTroops(true);

            // Upravo je potrosen jedan zlatnik
            setGold(getGold() - 1);

            // Samo na jednom cvoru se vrsi mobilizacija, pa je funkcija zavrsila sa radom
            return;
        }
    }
}

/**
 * @brief GridBot::closestTargetFloydWarshal
 * @param gridSize Duzina niza (grida)
 * @param borderTiles Granicni cvorovi bota
 * @param targets Ciljni cvorovi za bota
 * @return Indeks sledeceg cvora koji bot zeli da osvoji
 *
 * Primenjuje se Floyd-Warshal algoritam najkracih puteva
 * za pronalazenje svih puteva u grafu. Potom se bira najkraci
 * put izmedju granicnih cvorova i ciljeva, odnosno prva meta na
 * tom putu.
 */
int GridBot::closestTargetFloydWarshal(const int gridSize, const QVector<int> &borderTiles, const QVector<int> &targets)
{
    // Tezinska matrica za Floyd-Warshal algoritam
    QVector<QVector<int>> distances(gridSize, QVector<int>(gridSize, std::numeric_limits<int>().max()));
    // Matrica putanja: (i,j) - pretposlednji na putu od i-tog do j-tog cvora se nalazi u paths[i][j]
    QVector<QVector<int>> paths(gridSize, QVector<int>(gridSize, -1));

    // Tezine i "roditelji" za pocetne cvorove
    for(auto start : borderTiles) {
        distances[start][start] = 0;
        paths[start][start] = 0;
    }

    // Obradjuju se direktne grane iz matrice susedstva
    for(auto i=0; i<m_adjacencyList.size(); i++) {
        for(auto j=0; j<m_adjacencyList[i].size(); j++) {
            int k = m_adjacencyList[i][j].first;
            int defense = m_adjacencyList[i][j].second;

            distances[i][k] = defense;
            paths[i][k] = i;
        }
    }

    // Floyd-Warshall
    for(auto k=0; k<gridSize; k++) {
        for(auto i=0; i<gridSize; i++) {
            for(auto j=0; j<gridSize; j++) {
                if(distances[i][k] == std::numeric_limits<int>().max()
                   || distances[k][j] == std::numeric_limits<int>().max()) {
                    // Ne postoji grana i->k ili k->j
                    continue;
                }

                if(distances[i][k] + distances[k][j] < distances[i][j]) {
                    // Pronadjen je bolji put i->j i to preko cvora k
                    distances[i][j] = distances[i][k] + distances[k][j];
                    paths[i][j] = k;
                }
            }
        }
    }

    // Pronalazi se najbolji put od pocetnih do ciljnih cvorova
    // tj najbolji pocetni i najbolji ciljni cvor
    int bestStart = borderTiles[0];
    int bestTarget = targets[0];
    for(auto start : borderTiles) {
        for(auto target : targets) {
            if(distances[start][target] < distances[bestStart][bestTarget]) {
                bestStart = start;
                bestTarget = target;
            }
        }
    }

    // Pronadjen je put do ciljnog cvora, ali nam treba prvi na tom putu
    // kojeg treba osvojiti
    int closestTarget = bestTarget;
    while(paths[bestStart][closestTarget] != bestStart) {
        // Sve dok se ne radi o direktnoj grani
        closestTarget = paths[bestStart][closestTarget];
    }

    return closestTarget;
}

int GridBot::getMaxArmies() const
{
    return m_maxArmies;
}

void GridBot::setMaxArmies(int maxArmies)
{
    m_maxArmies = maxArmies;
}

int GridBot::getGold() const
{
    return m_gold;
}

void GridBot::setGold(int gold)
{
    m_gold = gold;
}

int GridBot::getArmies() const
{
    return m_armies;
}

void GridBot::setArmies(int armies)
{
    m_armies = armies;
}
