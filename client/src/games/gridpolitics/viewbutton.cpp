#include "../../../headers/games/gridpolitics/viewbutton.h"

ViewButton::ViewButton(qreal x, qreal y, qreal width, qreal height, const QString& text, const ViewType& viewType, const QColor& color)
    : MenuButton(x, y, width, height, MenuButton::Custom, text, color)
    , m_type(viewType)
{
}

void ViewButton::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    // Samo prihvatamo dogadjaj da bi se aktivirao mouseReleaseEvent
    QGraphicsItem::mousePressEvent(event);
    event->accept();
}

void ViewButton::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);

    // Samo se prosledjuje tip pogleda
    emit chooseView(m_type);
    event->accept();
}
