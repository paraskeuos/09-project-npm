#include "../../../headers/games/gridpolitics/gridpolitics.h"

#include <headers/games/gridpolitics/tile.h>

#include <QPushButton>

#include <headers/games/paras_menuitem_lib/exitbutton.h>
#include <headers/games/paras_menuitem_lib/gameoverpanel.h>
#include <headers/games/paras_menuitem_lib/messagefield.h>

GridPolitics::GridPolitics(QGraphicsView* parent, GameItem::GameId gameId)
    : Game(parent, gameId)
    , m_multiplayer(false)
    , m_gameOver(true)
    , m_gridSet(false)
    , m_tilePanel(nullptr)
    , m_robot(nullptr)
    , m_instrPanel(nullptr)
    , m_pickedView(ViewButton::General)
    , m_gold(0)
    , m_armies(0)
    , m_maxArmies(0)
    , m_upgradeBtn(nullptr)
    , m_mobilizeBtn(nullptr)
    , m_reinforceBtn(nullptr)
    , m_occupyBtn(nullptr)
    , m_nextTurnBtn(nullptr)
{
    const GameClient* client = gameClient();
    connect(client, &GameClient::connected, this, &GridPolitics::connectedToServer);
    connect(client, &GameClient::disconnected, this, &GridPolitics::disconnectedFromServer);
    connect(client, &GameClient::error, this, &GridPolitics::onConnectionError);
    connect(client, &GameClient::gameInit, this, &GridPolitics::gameInit);
    connect(client, &GameClient::opponentMove, this, &GridPolitics::opponentMove);
    connect(client, &GameClient::newGameOffer, this, &GridPolitics::onNewGameOffer);
    connect(client, &GameClient::gameInterrupted, this, &GridPolitics::onGameInterrupted);

    scene()->setBackgroundBrush(QBrush("#7bb1d1"));
    setMainMenu();
}

GridPolitics::~GridPolitics()
{
    if(m_robot) {
        m_robot->deleteLater();
    }
}

void GridPolitics::setGameScene()
{
    scene()->clear();
    initGame();
}

void GridPolitics::onTileSelected(const int i)
{
    setPickedTile(i);
    setTilePanel();
}

/**
 * @brief GridPolitics::upgrade
 * Povecava level izabranog cvora za 1.
 */
void GridPolitics::upgrade()
{
    // Povecava se level i azurira se ispis
    int currLvl = m_grid->getTile(pickedTile())->level();
    m_grid->getTile(pickedTile())->level(currLvl + 1);

    if(player() == currentPlayer()) {
        m_tilePanel->setLevel(m_tilePanel->level() + 1);

        // Smanjuje se broj zlatnika i azurira se ispis
        setGold(gold() - 1);
        m_gameBar->setCurrGold(gold());

        setTilePanel();

        // U slucaju MP, belezi se potez za kasnije slanje protivniku
        if(multiplayer()) {
            QJsonObject move;
            move[tr("upgrade")] = pickedTile();
            m_actions.push_back(move);
        }

        scene()->update();
    }
}

/**
 * @brief GridPolitics::mobilize
 *
 * Mobilizuju se trupe od izabranog cvora i
 * taj broj se dodaje na ukupan broj mobilizovanih trupa
 */
void GridPolitics::mobilize()
{
    // Povecava se broj trupa i azurira ispis
    setArmies(armies() + m_grid->getTile(pickedTile())->battalions());
    m_grid->getTile(pickedTile())->mobilizedTroops(true);
    m_tilePanel->setMobilizedTroops(true);
    m_gameBar->setCurrArmies(armies());

    // Smanjuje se broj zlatnika i azurira se ispis
    setGold(gold() - 1);
    m_gameBar->setCurrGold(gold());

    setTilePanel();

    scene()->update();
}

/**
 * @brief GridPolitics::reinforce
 *
 * Povecava defense vrednost izabranog cvora
 */
void GridPolitics::reinforce()
{
    // Povecava se defense vrednost za 10 i azurira ispis
    auto tile = m_grid->getTile(pickedTile());
    int currDefense = tile->defense();
    tile->defense(currDefense + 10);

    if(player() == currentPlayer()) {
        m_tilePanel->setDefense(currDefense + 10);

        // Smanjuje se broj zlatnika i azurira se ispis
        setGold(gold() - 1);
        m_gameBar->setCurrGold(gold());

        setTilePanel();

        // U slucaju MP, belezi se potez za kasnije slanje protivniku
        if(multiplayer()) {
            QJsonObject move;
            move[tr("reinforce")] = pickedTile();
            m_actions.push_back(move);
        }

        scene()->update();

    }
}

/**
 * @brief GridPolitics::occupy
 *
 * Okupira se susedni cvor ukoliko je to moguce
 */
void GridPolitics::occupy()
{
    Tile* tile = m_grid->getTile(pickedTile());

    // Okupira se cvor
    tile->owner(currentPlayer());

    if(player() == currentPlayer()) {
        // Igrac je zrtvovao onoliko trupa mobilizovane vojske koliko
        // iznosi defense osvojenog cvora
        setArmies(armies() - tile->defense());
        m_gameBar->setCurrArmies(armies());

        // Proverava se da li cvor sadrzi zlatnik i u tom
        // slucaju se azurira ispis u gamebar-u
        if(tile->hasGold()) {
            m_gameBar->setGoldPerTurn(m_gameBar->goldPerTurn() + 1);
        }

        // Smanjuje se broj zlatnika i azurira se ispis
        setGold(gold() - 1);
        m_gameBar->setCurrGold(gold());

        setTilePanel();

        // U slucaju MP, belezi se potez za kasnije slanje protivniku
        if(multiplayer()) {
            QJsonObject move;
            move[tr("occupy")] = pickedTile();
            m_actions.push_back(move);
        }
    }

    // Posle okupacije se mora azurirati stanje cvorova sto se tice pogleda
    onChooseView(pickedView());
}

/**
 * @brief GridPolitics::onNextTurn
 *
 * Slot koji reaguje na klik dugmeta NEXT TURN
 * Osvezava se stanje igre.
 */
void GridPolitics::onNextTurn()
{
    // Drugi igrac je na potezu, disabluje se Next Turn dugme
    m_nextTurnBtn->setEnabled(false);

    // Brise se tilePanel
    removeTilePanel();

    currentPlayer(player() == Player1 ? Player2 : Player1);

    // Proverava se da li je igri dosao kraj
    auto winner = m_grid->checkState();
    if(winner != Invalid) {
        // Blokira se dalja interakcija sa gridom ako je kraj igre
        m_gameOver = true;
        setGameOverPanel(winner);
    }

    if(multiplayer()) {
        // Za multiplayer se odigrani potezi pakuju
        // u JSON poruku i salju protivniku
        QJsonObject move;
        move[tr("actions")] = m_actions;

        // Prazne se nizovi za sledeci potez
        while(!m_actions.empty()) {
            m_actions.pop_back();
        }

        sendMove(move, winner);

    } else if(!m_gameOver){
        // Racunar odigrava potez
        m_robot->start();
    }

    scene()->update();
}

void GridPolitics::setTilePanel()
{
    removeTilePanel();

    // Ako je kraj igre, TilePanel se vise ne iscrtava
    if(m_gameOver) {
        return;
    }

    auto tile = m_grid->getTile(pickedTile());
    if(!tile->isLand()) {
        return;
    }

    const qreal width = 300;
    const qreal height = 125;
    const qreal topLeftX = scene()->sceneRect().right() - width;
    const qreal topLeftY = scene()->sceneRect().bottom() - height;

    m_tilePanel = new TilePanel(topLeftX, topLeftY, width, height,
                                tile->level(), tile->battalions(), tile->defense());
    m_tilePanel->setMobilizedTroops(tile->mobilizedTroops());
    scene()->addItem(m_tilePanel);

    // Dodaju se dugmici
    setTilePanelButtons();
}

/**
 * @brief GridPolitics::removeTilePanel
 *
 * Brise se TilePanel ukoliko je iscrtan.
 */
void GridPolitics::removeTilePanel()
{
    if(m_tilePanel) {
        m_tilePanel->deleteLater();
        m_tilePanel = nullptr;
    }
    if(m_upgradeBtn) {
        m_upgradeBtn->deleteLater();
        m_upgradeBtn = nullptr;
    }
    if(m_mobilizeBtn) {
        m_mobilizeBtn->deleteLater();
        m_mobilizeBtn = nullptr;
    }
    if(m_reinforceBtn) {
        m_reinforceBtn->deleteLater();
        m_reinforceBtn = nullptr;
    }
    if(m_occupyBtn) {
        m_occupyBtn->deleteLater();
        m_occupyBtn = nullptr;
    }
}

/**
 * @brief GridPolitics::initGame
 *
 * Vrse se inicijalne postavke za igru,
 * i crtanje elemenata i inicijalizacija back end-a.
 */
void GridPolitics::initGame()
{
    currentPlayer(Player1);
    m_gameOver = false;
    setGold(0);
    setArmies(0);
    setMaxArmies(0);

    if(multiplayer()) {
        // U multiplayer-u igraci moraju prvo da razmene podatke
        // o postavci grida. Pocetak igre pre toga nije moguc.
        m_gridSet = false;
    }

    if(!multiplayer() || player() == Player1) {
        // Dodaje se grid na scenu
        m_grid = new TileGrid();
        m_grid->generateGrid();

        connect(m_grid, &TileGrid::tileSelected, this, &GridPolitics::onTileSelected);

        scene()->addItem(m_grid);

        int goldPerTurn = m_grid->getGoldPerTurn(player());
        if(player() == currentPlayer()) {
            setGold(gold() + goldPerTurn);
        }
        setMaxArmies(m_grid->getMaxArmies(player()));

        // Postavlja se GameBar na vrhu prozora
        m_gameBar = new GameBar(scene()->sceneRect().left(), scene()->sceneRect().top(),
                                scene()->sceneRect().width(), 40);
        m_gameBar->setCurrGold(gold());
        m_gameBar->setGoldPerTurn(goldPerTurn);
        m_gameBar->setMaxArmies(m_maxArmies);

        scene()->addItem(m_gameBar);

        // Dodaje se Quit dugme u gornjem desnom uglu
        MenuButton* quitBtn = new MenuButton(scene()->sceneRect().right() - 70, scene()->sceneRect().top() + 5,
                                             60, 30, MenuButton::BackToMenu, tr("Quit"), QColor("#7bb1d1"));
        scene()->addItem(quitBtn);
        connect(quitBtn, &MenuButton::backToSPMenu, this, &GridPolitics::setMainMenu);

        // Dodaje se Instructions dugme pored Quit dugmeta
        MenuButton* instrBtn = new MenuButton(scene()->sceneRect().right() - 170, scene()->sceneRect().top() + 5,
                                              90, 30, MenuButton::ShowInstructions, tr("Instructions"), QColor("#7bb1d1"));
        scene()->addItem(instrBtn);
        connect(instrBtn, &MenuButton::showInstructions, this, &GridPolitics::onShowInstructions);

        // Dodaju se dugmici za poglede u gornjem levom uglu gamebara
        setViewButtons();

        // U slucaju multiplayera, drugom igracu se salju generisani podaci o gridu
        if(multiplayer()) {
            sendGeneratedGrid();
        } else {
            if(m_robot) {
                m_robot->deleteLater();
            }
            // U single playeru se inicijalizuje GridBot i povezuje njegov signal
            m_robot = new GridBot(Player2, m_grid, this);
            connect(m_robot, &GridBot::robotMadeMove, this, &GridPolitics::onRobotMove);
        }
    }

    // Postavlja se dugme za sledeci potez
    m_nextTurnBtn = new QPushButton(tr("NEXT TURN"));
    m_nextTurnBtn->resize(100, 50);
    if(player() != currentPlayer() || (multiplayer() && !m_gridSet)) {
        // Ako igrac nije na redu, ne moze odigrati potez
        // ili u multiplayer-u nisu razmenjeni podaci o generisanoj mapi
        m_nextTurnBtn->setEnabled(false);
    }

    m_nextTurnBtn->move(-m_nextTurnBtn->size().width()/2, scene()->sceneRect().top() + m_nextTurnBtn->height());

    scene()->addWidget(m_nextTurnBtn);

    connect(m_nextTurnBtn, &QPushButton::clicked, this, &GridPolitics::onNextTurn);
}

/**
 * @brief GridPolitics::setTilePanelButtons
 *
 * Crtaju se odgovarajuci dugmici preko TilePanela.
 */
void GridPolitics::setTilePanelButtons()
{
    const qreal width = 300;
    const qreal height = 125;
    const qreal topLeftX = scene()->sceneRect().right() - width;
    const qreal topLeftY = scene()->sceneRect().bottom() - height;

    Tile* tile = m_grid->getTile(pickedTile());
    if(player() == tile->owner()) {
        // Ako je celija u posedu, moze se unapredjivati, mobilizovati i sl
        m_upgradeBtn = new QPushButton(tr("UPGRADE"));
        m_upgradeBtn->move(topLeftX + 2*width/3,
                         topLeftY + height/4);
        scene()->addWidget(m_upgradeBtn);
        // Igrac nije na redu ili nema resurse za upgrade
        if(player() != currentPlayer() || m_gold == 0) {
            m_upgradeBtn->setEnabled(false);
        }
        connect(m_upgradeBtn, &QPushButton::clicked, this, &GridPolitics::upgrade);

        m_mobilizeBtn = new QPushButton(tr("RAISE"));
        m_mobilizeBtn->move(topLeftX + 2*width/3,
                         topLeftY +  + 2*height/4);
        scene()->addWidget(m_mobilizeBtn);

        // Igrac nije na redu, nema resurse za mobilizaciju ili ih je ovde vec mobilisao,
        // ili bi bio prestignut maksimum
        if(player() != currentPlayer() || m_gold == 0 || tile->mobilizedTroops() || armies() + tile->battalions() > maxArmies()) {
            m_mobilizeBtn->setEnabled(false);
        }

        connect(m_mobilizeBtn, &QPushButton::clicked, this, &GridPolitics::mobilize);

        m_reinforceBtn = new QPushButton(tr("REINFORCE"));
        m_reinforceBtn->move(topLeftX + 2*width/3,
                         topLeftY + 3*height/4);
        scene()->addWidget(m_reinforceBtn);

        // Igrac nije na redu, igrac nema resurse za utvrdjenje ili je dostignut maksimum
        if(player() != currentPlayer() || m_gold == 0 || tile->defense() + 10 == tile->maxDefense()) {
            m_reinforceBtn->setEnabled(false);
        }

        connect(m_reinforceBtn, &QPushButton::clicked, this, &GridPolitics::reinforce);

    } else {
        // Inace je moguca samo okupacija
        m_occupyBtn = new QPushButton(tr("OCCUPY"));
        m_occupyBtn->move(topLeftX + 2*width/3,
                         topLeftY + 3*height/4);
        scene()->addWidget(m_occupyBtn);

        // Igrac nije na redu, nema resurse za okupiranje,
        // ili nije sused tom cvoru,
        // ili nema dovoljno vojnika za napad
        if(player() != currentPlayer() || m_gold == 0 || !m_grid->isNeighbor(pickedTile(), player()) || m_armies < tile->defense()) {
            m_occupyBtn->setEnabled(false);
        }

        connect(m_occupyBtn, &QPushButton::clicked, this, &GridPolitics::occupy);
    }
}

/**
 * @brief GridPolitics::refreshTurn
 *
 * Poziva se pred pocetak svakog poteza.
 * Osvezavaju se zlatnici, cvorovi u posedu i slicno.
 */
void GridPolitics::refreshTurn()
{
    if(m_gameOver) {
        // Kraj igre
        return;
    }

    // Dobijaju se novi zlatnici
    int goldPerTurn = m_grid->getGoldPerTurn(player());
    setGold(gold() + goldPerTurn);

    // Trazi se trenutni maksimum za vojsku
    setMaxArmies(m_grid->getMaxArmies(player()));

    // Postavlja se GameBar na vrhu prozora
    m_gameBar->setCurrGold(gold());
    m_gameBar->setGoldPerTurn(goldPerTurn);
    m_gameBar->setMaxArmies(m_maxArmies);

    // Mobilizacija je opet moguca na svim cvorovima
    m_grid->refreshMobilize();

    m_nextTurnBtn->setEnabled(true);
}

/**
 * @brief GridPolitics::playOpponentMove
 * @param move QJsonValue objekat sa protivnikovim akcijama u potezu
 *
 * Funkcija odigrava sve protivnikove akcija u redosledu u kojem
 * ih je on izvrsio.
 */
void GridPolitics::playOpponentMove(const QJsonValue &move)
{
    // Odigravaju se sve protivnikove akcije, redom
    auto moveArr = move.toArray();
    while(!moveArr.empty()) {
        auto action = moveArr.first().toObject();
        moveArr.pop_front();

        if(action.contains(tr("upgrade"))) {
            auto actionVal = action[tr("upgrade")];
            if(actionVal.isNull() || !actionVal.isDouble()) {
                disconnectFromServer();

                setConnectScene(tr("Error: Network packets corrupted."), true);
            }

            setPickedTile(actionVal.toInt());
            upgrade();

        } else if(action.contains(tr("reinforce"))) {
            auto actionVal = action[tr("reinforce")];
            if(actionVal.isNull() || !actionVal.isDouble()) {
                disconnectFromServer();

                setConnectScene(tr("Error: Network packets corrupted."), true);
            }

            setPickedTile(actionVal.toInt());
            reinforce();

        } else if(action.contains(tr("occupy"))) {
            auto actionVal = action[tr("occupy")];
            if(actionVal.isNull() || !actionVal.isDouble()) {
                disconnectFromServer();

                setConnectScene(tr("Error: Network packets corrupted."), true);
            }

            setPickedTile(actionVal.toInt());
            occupy();
        }
    }
}

bool GridPolitics::multiplayer() const
{
    return m_multiplayer;
}

void GridPolitics::setMultiplayer(bool multiplayer)
{
    m_multiplayer = multiplayer;
}

int GridPolitics::pickedTile() const
{
    return m_pickedTile;
}

void GridPolitics::setPickedTile(int pickedTile)
{
    m_pickedTile = pickedTile;
}

int GridPolitics::armies() const
{
    return m_armies;
}

int GridPolitics::maxArmies() const
{
    return m_maxArmies;
}

void GridPolitics::setMaxArmies(int maxArmies)
{
    m_maxArmies = maxArmies;
}

void GridPolitics::setGold(int gold)
{
    m_gold = gold;
}

void GridPolitics::setArmies(int armies)
{
    m_armies = armies;
}

int GridPolitics::gold() const
{
    return m_gold;
}

void GridPolitics::connectedToServer()
{
    /*
     * Pokrece se kada se povezemo sa serverom.
     *
     * Ukoliko zelimo da pokrenemo igru u multiplayeru,
     * ovde treba pozvati initNewGame, tako trazimo od
     * servera da nas smesti u red cekanja za nasu igru.
    */
    setConnectScene(tr("Connected, waiting in queue..."), false);
    initNewGame();
}

void GridPolitics::disconnectedFromServer()
{
    /*
     * Metod ce biti pozvan ukoliko dodje do
     * "regularnog" prekida konekcije
     * (mi prekidamo vezu, server se ugasio i sl).
    */
    setConnectScene(tr("Disconnected from server"), true);
}

void GridPolitics::gameInit(int player, int gameIndex)
{
    /*
     * Vec cekamo u redu za igru, server je uspeo
     * da nas upari sa nekim igracem.
     * Odredio je ko je koji igrac.
     *
     * Ulazni argumenti sluze samo da se proslede
     * verziji funkcije od nadklase, ona sve podesava.
     *
     * Jedino sto treba uciniti je implementirati je
     * reagovanje na pocetak igre.
    */

    Game::gameInit(player, gameIndex);

    /*
     * Ovde mozemo npr. obavestiti da nas je server
     * upario sa drugim igracem i igra je pocela.
    */

    setGameScene();
}

void GridPolitics::opponentMove(const QJsonObject &move)
{
    /*
     * Drugi igrac nam je prosledio svoj potez
     * ili generisane podatke za grid ako smo mi drugi igrac
    */
    if(!m_gridSet) {
        // Grid jos nije generisan kod drugog igraca
        // Prvi igrac ceka potvrdu da je primenio podatke koji
        // su mu poslati - handshake

        if(player() == Player1) {
            // Prvi igrac je dobio potvrdu, igra moze poceti
            m_gridSet = true;

            // Azurira se enabled stanje dugmica
            m_nextTurnBtn->setEnabled(true);
            scene()->update();

            return;
        }

        // Drugi igrac raspakuje generisane podatke o gridu
        const QJsonValue gridVal = move.value("gridData");
        if(gridVal.isNull() || !gridVal.isArray()) {
            // Nisu dospeli validni podaci
            disconnectFromServer();
            setConnectScene(tr("Error: Network packets corrupted."), true);

            return;
        }

        // Kreira se grid i dodaju generisani podaci
        m_grid = new TileGrid();
        applyGeneratedGrid(gridVal);

        connect(m_grid, &TileGrid::tileSelected, this, &GridPolitics::onTileSelected);

        scene()->addItem(m_grid);

        int goldPerTurn = m_grid->getGoldPerTurn(player());
        if(player() == currentPlayer()) {
            setGold(gold() + goldPerTurn);
        }
        setMaxArmies(m_grid->getMaxArmies(player()));

        // Postavlja se GameBar na vrhu prozora
        m_gameBar = new GameBar(scene()->sceneRect().left(), scene()->sceneRect().top(),
                                scene()->sceneRect().width(), 40);
        m_gameBar->setCurrGold(gold());
        m_gameBar->setGoldPerTurn(goldPerTurn);
        m_gameBar->setMaxArmies(m_maxArmies);

        scene()->addItem(m_gameBar);

        // Dodaje se Quit dugme u gornjem desnom uglu
        MenuButton* quitBtn = new MenuButton(scene()->sceneRect().right() - 70, scene()->sceneRect().top() + 5,
                                             60, 30, MenuButton::BackToMenu, tr("Quit"), QColor("#7bb1d1"));
        scene()->addItem(quitBtn);
        connect(quitBtn, &MenuButton::backToSPMenu, this, &GridPolitics::setMainMenu);

        // Dodaje se Instructions dugme pored Quit dugmeta
        MenuButton* instrBtn = new MenuButton(scene()->sceneRect().right() - 170, scene()->sceneRect().top() + 5,
                                              90, 30, MenuButton::ShowInstructions, tr("Instructions"), QColor("#7bb1d1"));
        scene()->addItem(instrBtn);
        connect(instrBtn, &MenuButton::showInstructions, this, &GridPolitics::onShowInstructions);

        // Dodaju se dugmici za poglede u gornjem levom uglu gamebara
        setViewButtons();

        // Prvom igracu se salje "prazan potez" kao potvrda da je
        // primenio podatke i igra moze poceti - handshake
        QJsonObject handshakeOver;
        m_gridSet = true;
        sendMove(handshakeOver, Invalid);

        scene()->update();

        return;
    }

    // Obrada poteza ...
    const QJsonValue moveVal = move.value("actions");
    if(moveVal.isNull() || !moveVal.isArray()) {
        // Nije dospeo validan podatak o koloni
        disconnectFromServer();

        setConnectScene(tr("Error: Network packets corrupted."), true);

        return;
    }

    // Odigravaju se svi protivnikovi potezi, redom
    playOpponentMove(moveVal);

    // Protivnikov potez je odigran, proverava se da li je igri kraj
    auto winner = m_grid->checkState();

    if(winner == Invalid) {
        // Igri jos nije kraj

        // Osvezava se stanje igre pred novi potez
        refreshTurn();

        currentPlayer(currentPlayer() == Player1 ? Player2 : Player1);
    }

    scene()->update();
}

void GridPolitics::onNewGameOffer()
{
    setGameOverPanel(m_grid->checkState());
}

void GridPolitics::onGameInterrupted()
{
    /*
     * Igra je bila u toku i drugi igrac je
     * prekinuo vezu. Server nas je vratio u red za
     * cekanje i obavestava nas o tome.
    */

    // Obavestava se korisnik
    setConnectScene(tr("Your opponent disconnected. Waiting in queue..."), false);
    m_tilePanel = nullptr;
    m_upgradeBtn = nullptr;
    m_mobilizeBtn = nullptr;
    m_reinforceBtn = nullptr;
    m_occupyBtn = nullptr;
}

void GridPolitics::onConnectionError(QAbstractSocket::SocketError socketError)
{
    Game::error(socketError);
    QString errorMsg = errorMessage();
    if(errorMsg == nullptr) {
        return;
    }

    // Neka nepopravljiva greska sa vezom
    setConnectScene(errorMsg, true);
}

void GridPolitics::setGameOverPanel(const Game::Player &winner)
{
    // -1 - korisnik je izgubio
    // 0 - nereseno
    // 1 - pobeda
    int outcome;
    if(winner == player()) {
        outcome = 1;
    } else if (winner == Game::Draw) {
        outcome = 0;
    } else {
        outcome = -1;
    }

    const qreal panelWidth = 300;
    const qreal panelHeight = 200;

    QRectF panelRect = QRectF(-panelWidth/2, -panelHeight/2, panelWidth, panelHeight);
    GameOverPanel* panel = new GameOverPanel(panelRect, outcome, true);
    panel->setPos(0, -panelHeight);
    scene()->addItem(panel);

    const qreal btnWidth = 60;
    const qreal btnHeight = 40;
    const qreal btnGap = 50;

    MenuButton::ButtonType yesType = m_multiplayer ? MenuButton::PlayAgainMP : MenuButton::PlayAgainSP;
    MenuButton* yesBtn = new MenuButton(panelRect.center().x() - btnWidth - btnGap/2,
                                        panelRect.bottom() - btnHeight - btnGap/2,
                                        btnWidth, btnHeight, yesType, tr("Yes"), QColor("#7bb1d1"));
    yesBtn->setPos(0, -panelHeight);
    scene()->addItem(yesBtn);

    MenuButton::ButtonType noType = m_multiplayer ? MenuButton::NoPlayAgainMP : MenuButton::NoPlayAgainSP;
    MenuButton* noBtn = new MenuButton(panelRect.center().x() + btnGap/2,
                                       panelRect.bottom() - btnHeight - btnGap/2,
                                       btnWidth, btnHeight, noType, tr("No"), QColor("#7bb1d1"));
    noBtn->setPos(0, -panelHeight);
    scene()->addItem(noBtn);

    if(multiplayer()) {
        connect(yesBtn, &MenuButton::playAgainMP, this, &GridPolitics::playAgainMP);
        connect(noBtn, &MenuButton::noPlayAgainMP, this, &GridPolitics::noPlayAgainMP);

    } else {
        connect(yesBtn, &MenuButton::playAgainSP, this, &GridPolitics::playAgainSP);
        connect(noBtn, &MenuButton::noPlayAgainSP, this, &GridPolitics::noPlayAgainSP);
    }
}

void GridPolitics::playAgainSP()
{
    setGameScene();
}

void GridPolitics::noPlayAgainSP()
{
    setMainMenu();
}

void GridPolitics::playAgainMP()
{
    initNewGame();
    setConnectScene(tr("Waiting in queue..."), false);
}

void GridPolitics::noPlayAgainMP()
{
    setMainMenu();
}

ViewButton::ViewType GridPolitics::pickedView() const
{
    return m_pickedView;
}

void GridPolitics::setPickedView(const ViewButton::ViewType &pickedView)
{
    m_pickedView = pickedView;
}

void GridPolitics::setMainMenu()
{
    disconnectFromServer();
    scene()->clear();
    m_tilePanel = nullptr;
    m_upgradeBtn = nullptr;
    m_mobilizeBtn = nullptr;
    m_reinforceBtn = nullptr;
    m_occupyBtn = nullptr;

    // Dimenzije dugmica
    const qreal width = 250;
    const qreal height = 60;
    // Vertikalni prostor izmedju dugmica
    const qreal btnGap = 20;

    MenuButton* singlePlayerBtn = new MenuButton(-width/2, -height/2 - btnGap - height, width, height,
                                                 MenuButton::SinglePlayer, tr("Single Player"), QColor("#3c81aa"));
    scene()->addItem(singlePlayerBtn);
    connect(singlePlayerBtn, &MenuButton::chooseMode, this, &GridPolitics::onChooseMode);

    MenuButton* multiPlayerBtn = new MenuButton(-width/2, -height/2, width, height,
                                                MenuButton::MultiPlayer, tr("Play Online"), QColor("#3c81aa"));

    scene()->addItem(multiPlayerBtn);
    connect(multiPlayerBtn, &MenuButton::chooseMode, this, &GridPolitics::onChooseMode);

    ExitButton* exitBtn = new ExitButton(-width/2, -height/2 + btnGap + height, width, height, tr("Quit"), QColor("#3c81aa"));
    scene()->addItem(exitBtn);
}

/**
 * @brief Connect4::setConnectScene
 *
 * @param message Obavestenje o statusu konekcije
 * @param pen Boja teksta, podrazumevano crna
 *
 * Poziva se ako korisnik izabere multiplayer mod.
 * Postavlja scenu sa tekstom u vezi sa konekcijom na server,
 * obavestenje o tome da je igrac smesten u red za cekanje,
 * eventualno neka greska.
 * Takodje je tu i dugme za povratak na glavni meni.
 */
void GridPolitics::setConnectScene(const QString& message, const bool isError)
{
    scene()->clear();
    player(Invalid);

    const qreal width = 100;
    const qreal height = 40;
    // y-rastojanje od centra za polje i dugme Back
    const qreal distCenter = 10;

    // Obavestenje
    MessageField* msgField = new MessageField(-scene()->sceneRect().width()/4, -2*height - distCenter,
                                              scene()->sceneRect().width()/2, height, message, isError);
    scene()->addItem((msgField));

    // Dugme za povratak na glavni meni
    MenuButton* backToMenuBtn = new MenuButton(-width/2, -height/2 + distCenter + height, width, height,
                                                 MenuButton::BackToMenu, tr("Back"), QColor("#3c81aa"));
    scene()->addItem(backToMenuBtn);
    connect(backToMenuBtn, &MenuButton::backToSPMenu, this, &GridPolitics::setMainMenu);
}

/**
 * @brief GridPolitics::sendGeneratedGrid
 *
 * U slucaju multiplayera, prvi igrac generise grid
 * i ovom funkcijom salje generisane podatke u okviru "poteza"
 * za pocetni handshake pre nego sto igra moze da pocne.
 */
void GridPolitics::sendGeneratedGrid()
{
    QJsonArray gridData;
    const int gridSize = m_grid->sizeFactor()*m_grid->sizeFactor()
                         + (m_grid->sizeFactor() - 1)*(m_grid->sizeFactor() - 1);

    for(auto i = 0; i<gridSize; i++) {
        // Za svaki cvor se uzimaju podaci o tome da li je kopno
        // da li sadrzi zlatnik i njegovi susedi
        const auto tile = m_grid->getTile(i);
        QJsonObject tileData;
        tileData[tr("index")] = i;
        tileData[tr("isLand")] = tile->isLand();
        tileData[tr("hasGold")] = tile->hasGold();

        QJsonArray tileNeighbors;
        for(const auto neighbor : tile->neighbors()) {
            tileNeighbors.push_back(neighbor);
        }
        tileData[tr("neighbors")] = tileNeighbors;

        gridData.push_back(tileData);
    }

    // Salje se handshake poruka u okviru "poteza"
    QJsonObject message;
    message[tr("gridData")] = gridData;
    sendMove(message, Invalid);
}

/**
 * @brief GridPolitics::applyGeneratedGrid
 * @param gridData Sadrzi generisane podatke za grid
 *
 * Za svaki cvor u TileGrid se primenjuju generisani podaci
 * iz pristigle poruke.
 */
void GridPolitics::applyGeneratedGrid(const QJsonValue &gridData)
{
    const auto tiles = gridData.toArray();
    for(const auto tileSettings : tiles) {
        const auto tileData = tileSettings.toObject();

        // Trazi se indeks cvora
        if(!tileData.contains(tr("index")) || tileData[tr("index")].isNull() || !tileData[tr("index")].isDouble()) {
            disconnectFromServer();
            setConnectScene(tr("Error: Network packets corrupted."), true);
        }
        const auto index = tileData[tr("index")].toInt();
        const auto tile = m_grid->getTile(index);

        // Trazi se status kopna za cvor
        if(!tileData.contains(tr("isLand")) || tileData[tr("isLand")].isNull() || !tileData[tr("isLand")].isBool()) {
            disconnectFromServer();
            setConnectScene(tr("Error: Network packets corrupted."), true);
        }
        const auto isLand = tileData[tr("isLand")].toBool();
        tile->isLand(isLand);

        // Trazi se da li cvor sadrzi zlatnik
        if(!tileData.contains(tr("hasGold")) || tileData[tr("hasGold")].isNull() || !tileData[tr("hasGold")].isBool()) {
            disconnectFromServer();
            setConnectScene(tr("Error: Network packets corrupted."), true);
        }
        const auto hasGold = tileData[tr("hasGold")].toBool();
        tile->hasGold(hasGold);

        // Traze se susedi cvora
        if(!tileData.contains(tr("neighbors")) || tileData[tr("neighbors")].isNull() || !tileData[tr("neighbors")].isArray()) {
            disconnectFromServer();
            setConnectScene(tr("Error: Network packets corrupted."), true);
        }

        // Prolazi se kroz listu suseda
        const auto neighbors = tileData[tr("neighbors")].toArray();
        for(const auto neighbor : neighbors) {
            if(neighbor.isNull() || !neighbor.isDouble()) {
                disconnectFromServer();
                setConnectScene(tr("Error: Network packets corrupted."), true);
            }
            tile->addNeighbor(neighbor.toInt());
        }
    }
}

/**
 * @brief GridPolitics::setViewButtons
 *
 * Postavlja dugmice za poglede u gornji levi deo gamebara.
 */
void GridPolitics::setViewButtons()
{
    const qreal viewBtnWidth = 60;
    const qreal viewBtnHeight = 30;
    const qreal viewBtnGap = 10;

    ViewButton* generalViewBtn = new ViewButton(scene()->sceneRect().left() + viewBtnGap, scene()->sceneRect().top() + 5,
                                                viewBtnWidth, viewBtnHeight, tr("General"), ViewButton::General, QColor("#7bb1d1"));
    scene()->addItem(generalViewBtn);
    connect(generalViewBtn, &ViewButton::chooseView, this, &GridPolitics::onChooseView);

    ViewButton* levelViewBtn = new ViewButton(scene()->sceneRect().left() + 2*viewBtnGap + viewBtnWidth, scene()->sceneRect().top() + 5,
                                                viewBtnWidth, viewBtnHeight, tr("Level"), ViewButton::Level, QColor("#7bb1d1"));
    scene()->addItem(levelViewBtn);
    connect(levelViewBtn, &ViewButton::chooseView, this, &GridPolitics::onChooseView);

    ViewButton* armyViewBtn = new ViewButton(scene()->sceneRect().left() + 3*viewBtnGap + 2*viewBtnWidth, scene()->sceneRect().top() + 5,
                                                viewBtnWidth, viewBtnHeight, tr("Armies"), ViewButton::Army, QColor("#7bb1d1"));
    scene()->addItem(armyViewBtn);
    connect(armyViewBtn, &ViewButton::chooseView, this, &GridPolitics::onChooseView);

    ViewButton* defenseViewBtn = new ViewButton(scene()->sceneRect().left() + 4*viewBtnGap + 3*viewBtnWidth, scene()->sceneRect().top() + 5,
                                                viewBtnWidth, viewBtnHeight, tr("Defense"), ViewButton::Defense, QColor("#7bb1d1"));
    scene()->addItem(defenseViewBtn);
    connect(defenseViewBtn, &ViewButton::chooseView, this, &GridPolitics::onChooseView);
}


void GridPolitics::onChooseMode(bool multiplayer)
{
    setMultiplayer(multiplayer);

    if(this->multiplayer()) {
        // Povezujemo se sa serverom
        setConnectScene(tr("Connecting to server..."), false);
        connectToServer();

    } else {
        // Single player
        player(Player1);
        currentPlayer(Player1);
        setGameScene();
    }
}

/**
 * @brief GridPolitics::onRobotMove
 *
 * Slot koji reaguje kada racunar odigra svoje poteze.
 */
void GridPolitics::onRobotMove()
{
    // Proverava se da li je igri dosao kraj
    auto winner = m_grid->checkState();
    if(winner != Invalid) {
        // Blokira se dalja interakcija sa gridom ako je kraj igre
        m_gameOver = true;
        setGameOverPanel(winner);
    } else {
        // Korisnik je na potezu
        // Osvezava se stanje igre pred novi potez
        refreshTurn();

        currentPlayer(Player1);
    }

    scene()->update();
}

void GridPolitics::onChooseView(const ViewButton::ViewType &view)
{
    switch(view) {
    case ViewButton::General: {
        m_grid->setGeneralView();
        break;
    }
    case ViewButton::Level:
    case ViewButton::Army:
    case ViewButton::Defense: {
        m_grid->setSpecificView(player(), view);
        break;
    }
    default:
        break;
    }

    setPickedView(view);
    scene()->update();
}

/**
 * @brief GridPolitics::onShowInstructions
 *
 * Korisnik je kliknuo na dugme Instructions,
 * prikazuju se uputstva za igru.
 */
void GridPolitics::onShowInstructions()
{
    if(m_instrPanel) {
        if(m_instrPanel->isVisible()) {
            // Korisnik je kliknuo na Instructions iako se vec vide
            return;
        }
        // Ako je vec ranije inicijalizovano,
        // u medjuvremenu je sakriveno pa se samo opet prikazuje.
        m_instrPanel->show();

    } else {
        QString instructions = tr("Everything in the game costs gold.\n\n"
                                  "If you click on one of your tiles, some actions will be\n"
                                  "available should you have at least 1 gold.\n\n"
                                  "Tile's level determines how many battalions you can raise\n"
                                  "on the tile in one turn, as well as its maximum defense value.\n\n"
                                  "If you click on a neighboring tile, you can occupy it if you have\n"
                                  "1 gold and enough mobilized battalions (check the bar at the top of\n"
                                  "the screen). You need to have at least as many battalions as the\n"
                                  "defense value of the tile you want to occupy.\n\n"
                                  "You win if your opponent loses their capital and has no other\n"
                                  "gold generating tiles in their possession.\n\n"
                                  "That should be enough to get you started. Now, onward to victory!");
        m_instrPanel = new InstructionsPanel(-scene()->sceneRect().width()/4,
                                             -scene()->sceneRect().height()/4,
                                             scene()->sceneRect().width()/2,
                                             scene()->sceneRect().height()/2,
                                             instructions);
        scene()->addItem(m_instrPanel);
    }

    // Prikazuje se dugme za skljanjanje uputstava
    MenuButton* closeInstrBtn = new MenuButton(-35, 125, 70, 30, MenuButton::CloseInstructions, tr("Close"), QColor("#7bb1d1"));
    scene()->addItem(closeInstrBtn);
    connect(closeInstrBtn, &MenuButton::closeInstructions, this, &GridPolitics::onCloseInstructions);
}

/**
 * @brief GridPolitics::onCloseInstructions
 *
 * Slot za sklanjaje prikazanih uputstava.
 */
void GridPolitics::onCloseInstructions()
{
    m_instrPanel->hide();
    // Brise se dugme Close sa scene
    sender()->deleteLater();
}
