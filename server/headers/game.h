#ifndef GAME_H
#define GAME_H

#include "serverworker.h"

#include <QGraphicsItem>



class Game : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Game)
public:
    enum GameId { Game1, Game2, Game3, Game4, Game5, Game6 };

    explicit Game(QObject* parent = nullptr);

    void gameId(const GameId &value);

    void player1(ServerWorker *value);

    void player2(ServerWorker *value);

    ServerWorker *player1() const;

    ServerWorker *player2() const;

private:
    GameId m_gameId;
    ServerWorker* m_player1;
    ServerWorker* m_player2;
};

#endif // GAME_H
