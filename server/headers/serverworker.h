#ifndef SERVERWORKER_H
#define SERVERWORKER_H

#include <QJsonObject>
#include <QObject>
#include <QTcpSocket>

//#include <qt5/QtNetwork/QTcpSocket>

class ServerWorker : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ServerWorker)
public:
    explicit ServerWorker(QObject *parent = nullptr);

    virtual bool setSocketDescriptor(qintptr socketDescriptor);
    void sendJson(const QJsonObject &json);

    int gameIndex() const;
    void gameIndex(int gameIndex);

    int gameChoice() const;
    void gameChoice(int gameChoice);

signals:
    void disconnectedFromClient();
    void error();
    void jsonReceived(QJsonObject jsonObj);

public slots:
    void disconnectFromClient();

private slots:
    void onReadyRead();

private:
    int m_gameIndex = -1;
    int m_gameChoice = -1;
    QTcpSocket* m_serverSocket;
};

#endif // SERVERWORKER_H
