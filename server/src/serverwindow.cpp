#include "../headers/serverwindow.h"
#include "./ui_serverwindow.h"

ServerWindow::ServerWindow(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::ServerWindow)
    , m_gameServer(new GameServer(this))
{
    ui->setupUi(this);

    connect(ui->startStopButton, &QPushButton::clicked, this, &ServerWindow::toggleStartServer);
    connect(m_gameServer, &GameServer::logMessage, this, &ServerWindow::logMessage);
}

ServerWindow::~ServerWindow()
{
    delete ui;
}

void ServerWindow::logMessage(const QString &msg)
{
    ui->logEditor->appendPlainText(msg);
}

void ServerWindow::toggleStartServer()
{
    if (m_gameServer->isListening()) {

        m_gameServer->stopServer();
        ui->startStopButton->setText(tr("Start Server"));
        logMessage(tr("Server stopped."));

    } else {
        if (!m_gameServer->listen(QHostAddress::Any, 1967)) {
            logMessage(tr("\nERROR: Unable to start server\n"));
            return;
        }

        logMessage(tr("Server started."));
        ui->startStopButton->setText(tr("Stop Server"));
    }
}
