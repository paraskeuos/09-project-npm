#include "../headers/gameserver.h"
#include <climits>

GameServer::GameServer(QObject* parent)
    : QTcpServer(parent)
{
    m_gameQueues.insert(Game::Game1, QList<ServerWorker*>());
    m_gameQueues.insert(Game::Game2, QList<ServerWorker*>());
    m_gameQueues.insert(Game::Game3, QList<ServerWorker*>());
    m_gameQueues.insert(Game::Game4, QList<ServerWorker*>());
    m_gameQueues.insert(Game::Game5, QList<ServerWorker*>());
    m_gameQueues.insert(Game::Game6, QList<ServerWorker*>());
}

/**
 * @brief GameServer::incomingConnection
 * @param handle
 * Stavlja novopovezanog klijenta u listu m_clients.
 */
void GameServer::incomingConnection(qintptr handle)
{
    ServerWorker* worker = new ServerWorker(this);

    if(!worker->setSocketDescriptor(handle)) {
        worker->deleteLater();
        return;
    }

    connect(worker, &ServerWorker::jsonReceived, this, &GameServer::jsonReceived);
    connect(worker, &ServerWorker::disconnectedFromClient,
            this, std::bind(&GameServer::playerDisconnected, this, worker));

    m_clients.append(worker);

    emit logMessage(tr("New client connected. (Total: %1)").arg(m_clients.size()));
}

/**
 * @brief GameServer::jsonReceived
 * @param jsonObj
 * Proverava tip pristigle JSON poruke i prosledjuje je odgovarajucoj funkciji.
 */
void GameServer::jsonReceived(QJsonObject jsonObj)
{
    const QJsonValue typeVal = jsonObj.value("type");
    if(typeVal.isNull() || !typeVal.isString()) {
        return;
    }

    if(typeVal.toString().compare("play", Qt::CaseInsensitive) == 0) {
        // Igraci razmunjuju poteze
        playMoveExchange(jsonObj);

    } else if(typeVal.toString().compare("pickGame", Qt::CaseInsensitive) == 0) {
        // Novopovezani klijent bira svoju igru
        onPickGame(jsonObj);
    }
}

void GameServer::playerDisconnected(ServerWorker *sender)
{
    //m_clients.removeAll(sender);
    emit logMessage(tr("A player has disconnected."));

    if(m_clients.contains(sender)) {

        // Klijent je u pocetnoj listi
        m_clients.removeOne(sender);

    } else if(sender->gameIndex() == -1) {

        // Klijent nije ni u pocetnoj listi niti se prekinula veza dok je igrao igru
        // To znaci da se nalazi u nekom redu za cekanje
        m_gameQueues[Game::GameId(sender->gameChoice())].removeOne(sender);

    } else {
        // Prekinula se veza dok je igrao igru. Unistavamo igru, obavestavamo drugog igraca
        // o tome i smestamo ga u red za cekanje
        onGameInterrupted(sender);
    }

    sender->deleteLater();
}

void GameServer::stopServer()
{
    // Prekidanje veze sa klijentima iz opste liste
    for (auto worker : m_clients) {
        worker->disconnectFromClient();
    }

    // Prekidanje veze sa svim klijentima koji cekaju u redu za neku igru
    for(auto gameQueue : m_gameQueues) {
        for(auto worker : gameQueue) {
            worker->disconnectFromClient();
        }
    }

    // Prekidanje svih igara u toku
    for(auto game : m_games.values()) {
        // Blokiraju se svi signali od oba igraca
        // da se ne bi aktivirao slot onGameInterrupted
        // (iza njega stoji drugacija logika)
        const QSignalBlocker blocker1(game->player1());
        const QSignalBlocker blocker2(game->player2());

        game->player1()->blockSignals(true);
        game->player2()->blockSignals(true);

        game->player1()->disconnectFromClient();
        game->player2()->disconnectFromClient();

    }
    m_games.clear();

    close();
}

void GameServer::sendJson(ServerWorker *destination, const QJsonObject &message)
{
    Q_ASSERT(destination);
    destination->sendJson(message);
}

/**
 * @brief GameServer::onGameInterrupted
 * @param gonePlayer
 *
 * Poziva se kada se prekine veza sa igracem koji je igrao neku igru.
 * Drugog igraca obavestava o dogadjaju i premesta ga u red za cekanje.
 */
void GameServer::onGameInterrupted(ServerWorker* gonePlayer)
{
    Game* game = m_games[gonePlayer->gameIndex()];
    ServerWorker* otherPlayer = gonePlayer == game->player1() ? game->player2() : game->player1();

    // Drugi igrac se premesta u red za cekanje
    if(otherPlayer) {
        otherPlayer->gameIndex(-1);
        m_gameQueues[Game::GameId(otherPlayer->gameChoice())].push_back(otherPlayer);
    }
    // Unistava se igra
    m_games.remove(gonePlayer->gameIndex());

    // Izlaz ako je u medjuvremenu i drugi igrac prekinuo vezu
    if(!otherPlayer) {
        return;
    }

    // Obavestava se drugi igrac o dogadjaju
    QJsonObject message;
    message[tr("type")] = tr("gameInterrupted");

    emit logMessage("Requeued other player, informing them.");

    sendJson(otherPlayer, message);

    // Proverava se da li se otherPlayer vec moze spojiti u igru sa nekim drugim iz reda
    tryGameStart(Game::GameId(otherPlayer->gameChoice()));
}

/**
 * @brief GameServer::tryGameStart
 * @param game
 *
 * Za datu igru, proverava da li postoje bar dva igraca koja cekaju u redu.
 * U potvrdnom ih izbacuje iz reda i poziva gameInit funkciju.
 * U odricnom ne radi nista.
 */
void GameServer::tryGameStart(Game::GameId game)
{
    if(m_gameQueues[game].size() < 2) {
        return;
    }

    // Postoji bar jos jedan igrac koji ceka na protivnika, uparuju se dva sa pocetka reda
    ServerWorker* player1 = m_gameQueues[game].first();
    m_gameQueues[game].pop_front();
    ServerWorker* player2 = m_gameQueues[game].first();
    m_gameQueues[game].pop_front();

    gameInit(player1, player2);
}

/**
 * @brief GameServer::gameInit
 * @param player1
 * @param player2
 *
 * Za nacetu igru, igracima salje poruku u tome ko je prvi a ko drugi igrac
 * i koji je serverski index njihove igre.
 */
void GameServer::gameInit(ServerWorker* player1, ServerWorker* player2)
{
    // Kreira novi objekat za igru
    Game* game = new Game(this);
    game->gameId(Game::Game1);
    game->player1(player1);
    game->player2(player2);

    // Odredjuje se gameIndex trazeci prvu slobodnu vrednost kljuca u m_games
    int newGameIndex = 0;
    for(auto i=0; i<std::numeric_limits<int>().max(); i++) {
        if(!m_games.contains(i)) {
            newGameIndex = i;
            break;
        }
    }

    // Igraci moraju da znaju koji je id njihove igre
    // Igra se stavlja u listu
    game->player1()->gameIndex(newGameIndex);
    game->player2()->gameIndex(newGameIndex);
    m_games.insert(newGameIndex, game);

    // Salju se poruke igracima o tome ko je koji i njihov gameId
    QJsonObject gameInitMessage;
    gameInitMessage[tr("gameIndex")] = newGameIndex;
    gameInitMessage[tr("type")] = tr("gameInit");
    gameInitMessage[tr("player")] = 1;

    emit logMessage("Sending gameInit message to Player 1");
    sendJson(player1, gameInitMessage);

    gameInitMessage[tr("player")] = 2;
    emit logMessage("Sending gameInit message to Player 2");
    sendJson(player2, gameInitMessage);
}

/**
 * @brief GameServer::playMoveExchange
 * @param jsonObj
 *
 * Poziva se na ako je jedan igrac napravio potez u igri.
 * Server prosledjuje poruku drugom igracu, ali i proverava
 * da li je doslo do kraja igre (u tom slucaju zapocinje dalju
 * razmenu poruka sa igracima.
 */
void GameServer::playMoveExchange(QJsonObject &jsonObj)
{
    const QJsonValue gameIndexVal = jsonObj.value("gameIndex");
    if(gameIndexVal.isNull() || !gameIndexVal.isDouble()) {
        emit logMessage(tr("Error: Can't see gameIndex."));
        return;
    }

    // Cita se gameIndex kako bi se pronasla igra u listi,
    // izmedju ostalog zbog pronalazenja primaoca kome se prosledjuje poruka.
    const int gameIndex = gameIndexVal.toInt();
    if(!m_games.contains(gameIndex)) {
        emit logMessage(tr("Error: Can't find game with id %1.").arg(gameIndex));
        return;
    }

    if(static_cast<ServerWorker*>(sender()) == m_games[gameIndex]->player1()) {
        sendJson(m_games[gameIndex]->player2(), jsonObj);
    } else {
        sendJson(m_games[gameIndex]->player1(), jsonObj);
    }

    const QJsonValue outcomeVal = jsonObj.value("outcome");

    if(outcomeVal.isNull() || !outcomeVal.isDouble()) {
        emit logMessage(tr("Error: Can't see game state."));
        return;
    }

    // outcome = 0       - igra je u toku
    // outcome = 1 ili 2 - pobedio je 1./2. igrac
    // outcome = 3       - nereseno
    const int outcome = outcomeVal.toInt();

    // Ako je zavrsena igra, pitaju se igraci da li zele revans
    if(outcome != 0) {
        emit logMessage(tr("Game %1 has ended. Sending playAgain messages.").arg(gameIndex));
        offerPlayAgain(gameIndex);
    }
}

/**
 * @brief GameServer::onPickGame
 * @param jsonObj
 *
 * Poziva se kada je igrac odabrao neku igru po povezivanju.
 * Igrac se pomera u odgovarajuci red gde ceka protivnika.
 * Ako vec ima neki drugi igrac koji ceka, spajaju se u igru.
 */
void GameServer::onPickGame(QJsonObject &jsonObj)
{
    const QJsonValue choiceVal = jsonObj.value("choice");

    if(choiceVal.isNull() || !choiceVal.isDouble()) {
        emit logMessage(tr("Error: Can't determine which game was picked."));
        return;
    }

    const Game::GameId choice = Game::GameId(choiceVal.toInt());
    ServerWorker* const player = static_cast<ServerWorker*>(sender());
    player->gameChoice(choiceVal.toInt());

    // Igrac se premesta iz liste cekanja i ubacuje u red za odgovarajucu igru.
    m_clients.removeOne(player);
    m_gameQueues[choice].push_back(player);

    // Proverava se da li se igrac moze spojiti sa nekim drugim iz reda
    // i u potvrdnom pokrece igru.
    tryGameStart(choice);
}

/**
 * @brief GameServer::offerPlayAgain
 * @param gameIndex
 *
 * U slucaju kraja igre, server pita igrace da li zele da igraju jos.
 * U medjuvremenu ih premesta u listu klijenata od kojih ceka izbor igre.
 */
void GameServer::offerPlayAgain(const int gameIndex)
{
    QJsonObject message;
    message[tr("type")] = tr("playAgain");

    ServerWorker* player1 = m_games[gameIndex]->player1();
    player1->gameIndex(-1);
    player1->gameChoice(-1);

    ServerWorker* player2 = m_games[gameIndex]->player2();
    player2->gameChoice(-1);
    player2->gameIndex(-1);

    m_clients.append(player1);
    m_clients.append(player2);
    m_games.remove(gameIndex);

    sendJson(player1, message);
    sendJson(player2, message);
}

