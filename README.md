# Project 09-Project-NPM

Апликација за играње разних мини игара у single player и multiplayer моду

## Developers

- [Немања Живановић, 89/2016](https://gitlab.com/NemanjaZivanovic)
- [Милан Радишић, 192/2016](https://gitlab.com/paraskeuos)
- [Петар Дамњановић, 115/2016](https://gitlab.com/lowzyyy)

## Izgradnja projekta

Pogledati sledeci [link](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/09-project-npm/-/wikis/%D0%A2%D1%83%D1%82%D0%BE%D1%80%D0%B8%D1%98%D0%B0%D0%BB-(build)).
